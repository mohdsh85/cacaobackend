/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function loadMoreComments(secureId,imageId)
{
    var currentPage=parseInt($("#currentPage").val());
    var totalPages=parseInt($("#totalPages").val());
     var nextPage=(currentPage)+(1);
     console.log(nextPage);
     $("#currentPage").val(nextPage); 
            $.post(baseUrl+"index.php/Comments/loadMoreComments",
            {
                secureImage: secureId,
                imageId: imageId,
                offset:nextPage

            },
            function(data, status){

//                   var h=$(".swal-wide").height()+200;
//                   console.log(h);
//                   $(".swal-wide").attr('height',(h));
                   $("#"+secureId).append(data);
                    $(".list-group").scrollTop()
                })

                if(totalPages==nextPage)
                    $("#loadButton").attr('style','display:none');
}
function renderImagePopUp(secureImage,caption,imageId,token)
{

        $.post(baseUrl+"index.php/Clients/showImagePopup",
            {
                secureImage: secureImage,
                caption: caption,
                imageId:imageId,
                token:token
            },
            function(data, status){
                swal({
                title: caption,
                html: data,
//                imageUrl: secureImage,
//                imageWidth: 400,
//                imageHeight: 200,
//                imageAlt: 'Custom image',
//                animation: false,
                    width:'90%',
                   height:'600px',
                   customClass: 'swal-wide',
                   showCancelButton: false,
                     showConfirmButton:false
                    
                })

            });

}

function sendComment(imageId,secureId,need_login,fill_comment,write_comment)
{
     var commentTxt=$("#commentTxt").val();
     $("#commentTxt").val('') ;
    $.post(baseUrl+"index.php/Comments/writeComment",
            {
               
                secureImage: secureId,
                imageId:imageId,
                txt:commentTxt
            },
            function(data, status){
                if(data==0)
                {
                    swal({
                    title: write_comment,
                    html: need_login,
    //                imageUrl: secureImage,
    //                imageWidth: 400,
    //                imageHeight: 200,
    //                imageAlt: 'Custom image',
    //                animation: false,
                        width:'60%',
                       height:'100px',
                       customClass: 'swal-wide',
                       showCancelButton: false,
                         showConfirmButton:true

                    })                                   
                }else
                {
                        var currentComments=parseInt($("#totalComments").html());
                    console.log(currentComments);
                    var currentCommentTotal=(currentComments)+(1)  ;
                    $("#totalComments").html(currentCommentTotal); 
                    $("#"+secureId).html(data);
                }
                

            });
}