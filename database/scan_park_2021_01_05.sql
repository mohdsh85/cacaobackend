-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 05, 2021 at 09:38 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scan_park`
--

-- --------------------------------------------------------

--
-- Table structure for table `aswt_billing_token`
--

CREATE TABLE `aswt_billing_token` (
  `id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8_bin NOT NULL,
  `package_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `parsed` int(11) NOT NULL,
  `billing_status` varchar(100) COLLATE utf8_bin NOT NULL,
  `datetime` int(11) NOT NULL,
  `payment_reference` int(11) NOT NULL,
  `visa_trans_id` int(11) NOT NULL,
  `billing_completed_visa_side` int(11) NOT NULL,
  `amount` float NOT NULL,
  `currency` varchar(10) COLLATE utf8_bin NOT NULL,
  `reference_no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `aswt_billing_token`
--

INSERT INTO `aswt_billing_token` (`id`, `token`, `package_id`, `client_id`, `parsed`, `billing_status`, `datetime`, `payment_reference`, `visa_trans_id`, `billing_completed_visa_side`, `amount`, `currency`, `reference_no`) VALUES
(30, 'LvEE7HvS0meRR7LGmdULyZXcc26EFglr', 1, 11, 1, 'success', 1580246039, 0, 422632, 1, 5, 'USD', 0),
(31, 'uVOD2k6pZQiOg4IVjmja2tyB9yiWAM15', 1, 11, 1, 'success', 1580481847, 0, 423627, 1, 5, 'USD', 0),
(32, 'Esibpnage1949nUlysA7IOQ78LRBRwHw', 1, 11, 1, 'success', 1580486818, 0, 423633, 1, 5, 'USD', 0),
(33, 'XjCOCjqS1Wa1ysVbtIjDAUPFTd48UPZS', 1, 11, 1, 'success', 1580486921, 0, 423635, 1, 5, 'USD', 0),
(34, 'hxL9soJvypImzt6twrDpy81IJh6LFrAW', 1, 10, 1, 'success', 1580505779, 0, 423668, 1, 5, 'USD', 0),
(35, 'ym8cxztzj4IvJx4OHzvRqnOwUx8Ixtj6', 1, 13, 1, 'success', 1580506042, 0, 423669, 1, 5, 'USD', 0),
(36, 'VxPKG9ufev9qDtGZ4UxVcRgKcANMLGCH', 1, 14, 1, 'success', 1580565420, 0, 423797, 1, 5, 'USD', 0),
(37, 'myTyKZ8NiKhF64sjzgi684vvSD8r15Ln', 1, 11, 1, 'success', 1580567972, 0, 423808, 1, 5, 'USD', 0),
(38, 'PvBSzPJvK625ysidL65qPV6KZNlwfbU5', 2, 11, 1, 'success', 1580575310, 0, 423815, 1, 5, 'USD', 0),
(39, 'ZQEmsyRnkcOTlgmMcHZKzP0hkoOWzQXy', 1, 14, 1, 'success', 1580590671, 0, 423835, 1, 5, 'USD', 0),
(40, 'MzjxbVeGlHO7v0Atyeh7R8LZqb4MNTQz', 3, 14, 1, 'success', 1580591848, 0, 423836, 1, 8, 'USD', 0),
(41, 'oF8GdWul908PzmxjmBY8uDMuLesdVb7k', 1, 14, 1, 'success', 1580591964, 0, 423837, 1, 5, 'USD', 0),
(42, '4IPMLaTp9Ph4RPE96fCFT0zAcFGo13k6', 1, 14, 1, 'success', 1580592227, 0, 423838, 1, 5, 'USD', 0),
(43, 'NO6RZ5jgvFhyAa3rYOzIzDPcZTB6qatd', 1, 14, 1, 'success', 1580593136, 0, 423842, 1, 5, 'USD', 0),
(44, 'F2sYrNvmTu9ousPwN9fQMTzMAtjBVXcA', 1, 11, 1, 'success', 1580634535, 0, 423886, 1, 5, 'USD', 0),
(45, '0bFdaWGwZjfLsp1JYAappi6G0owqXZRX', 1, 14, 1, 'success', 1580675965, 0, 424016, 1, 5, 'USD', 0),
(46, 'RbMuuJuaKzkOAGoUJaejznpg1bE9MHNE', 1, 11, 1, 'success', 1580766149, 0, 424504, 1, 5, 'USD', 0),
(47, 'FIMeBfAczlrNchTNrWTUWMFGxXNxMzbr', 1, 11, 1, 'success', 1580766404, 0, 424505, 1, 5, 'USD', 0),
(48, 'KEu9V4zjS02dogwAZe82tpxhYfhki222', 1, 14, 1, 'success', 1580769125, 0, 424507, 1, 5, 'USD', 0),
(49, 'BBkCujwSfQGQe5nm92OXVuJFtehH4JmG', 3, 13, 1, 'success', 1580823781, 0, 424836, 1, 8, 'USD', 0),
(50, 'f69FVp3cJuUYfPiDXHpoWYMycLAm0cSf', 2, 13, 1, 'success', 1580823985, 0, 424840, 1, 5, 'USD', 0),
(51, 'dgWiC0RLsJQIBr0IlO66aeyF62TJoOHB', 1, 13, 1, 'success', 1580827913, 0, 424886, 1, 5, 'USD', 0),
(52, 'HRI0Lxo1ZZ0OGTCE0LFjuLZOkSt2He8p', 2, 14, 1, 'success', 1580837349, 0, 424935, 1, 5, 'USD', 0),
(53, 'QgfFfrM09Jw9cip0TmGieOiX9qpxc1t2', 1, 14, 1, 'success', 1581030580, 0, 425811, 1, 5, 'USD', 0),
(54, 'bSPvl4pDgz9fSh4vNpiIKLsaY4qZnp0y', 1, 14, 1, 'success', 1581030978, 0, 425812, 1, 5, 'USD', 0),
(55, 'VvT8fgrreuDvKubsgIimiYA05EkIxn8t', 1, 11, 1, 'success', 1581191466, 0, 426592, 1, 5, 'USD', 0),
(56, '92cPOm9zzQypSqtK60pWFztb1gwskW6u', 1, 14, 1, 'success', 1581459345, 0, 428023, 1, 5, 'USD', 0),
(57, 'dya4yuQfkT0tC0cnAgR9f8nELJasP5B3', 1, 11, 1, 'success', 1581535498, 0, 428466, 1, 5, 'USD', 0),
(58, 'HI4UBWwDzUx2mZR81ndBbp4kYhhnXLpF', 1, 14, 1, 'success', 1581540335, 0, 428479, 1, 5, 'USD', 0),
(59, 'gSzWJ4J4ua0y7CnaLyco7JVJAVoVYuGf', 1, 14, 1, 'success', 1581680709, 0, 429134, 1, 5, 'USD', 0),
(60, 'T7h8SPC10GpV4oQTA6qbh4nDIZOZ8mu1', 2, 13, 1, 'success', 1583320596, 0, 438499, 1, 5, 'USD', 0);

-- --------------------------------------------------------

--
-- Table structure for table `aswt_cc_info`
--

CREATE TABLE `aswt_cc_info` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `c_number` text COLLATE utf8_bin NOT NULL,
  `cvv` text COLLATE utf8_bin NOT NULL,
  `c_expiry_month` text COLLATE utf8_bin NOT NULL,
  `c_expiry_year` text COLLATE utf8_bin NOT NULL,
  `client_name` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `aswt_cc_info`
--

INSERT INTO `aswt_cc_info` (`id`, `client_id`, `c_number`, `cvv`, `c_expiry_month`, `c_expiry_year`, `client_name`) VALUES
(1, 2, 'a032e1fc55b818c12cbd200cd0456bcc1a7dab450e8a1f4babcae27d4266899628e50ed0d65b9ce3a342ad36ab0d8737f2d8f9a021ff769b809aab4df1f07164lIpfH7CPPGDB36w1OTdv60bXxwGnh6P48YpsBKp3t9Qm8iH1ShvHFq3/biXh5MM/', 'eef60d6223719ee0832e3980e8920cf21b73948d009dd3b9bf1ee97d7a10c9465f645017cdd73a6d4bbb3fef77e09babd3600d42c820271c057a31543284b5ddIl9K6sKZGdHduLzG9as2eavx9/0KqNGM0PHruSvrd+8=', '1fa66803e6f551182b01c4d15abe49f9c64dc5c8ff1cdec4622b64e2968481a2d46d40ecc7e68f00f763baf6ef68ef22d1d52ee44b2dca14fc543943c07bd877a/UwSCdezLa9wD3elMPk3IWZ0zht/DPYebOkZ2hL1cg=', 'cef09f86ebf208f67c473662229f2032713b20593d831adf4dff9c0beb26e620a121894627bd612cbeb07c17a0fe7146615da3ce86cebed745b8668053b07b58P4AsuQFfpl6kV86cg8qh1LuFhg8anJY9aVm6IDThc8Y=', '04a293fd0b9bd84904da17887b7ddfeb20c5eba0f86d7358cd3527ba974e08542aaeaae8ae5c8cbd932ed954cab5d8e5aa9a83c37426b86db26624a6dc429b116h9vnkOUpydXN8IdOfGzz/kQ3M5PppnbWtNl/RxVAAe3Lmuv016HF/3afL/teTfK');

-- --------------------------------------------------------

--
-- Table structure for table `aswt_clients`
--

CREATE TABLE `aswt_clients` (
  `id` int(11) NOT NULL,
  `social_id` varchar(250) COLLATE utf8_bin NOT NULL,
  `client_email` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `social_type` int(11) NOT NULL COMMENT '1 facebook, 2 gmail',
  `register_date` int(11) NOT NULL,
  `device_udid` text COLLATE utf8_bin,
  `device_token` text COLLATE utf8_bin,
  `user_name` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `full_name` varchar(220) COLLATE utf8_bin NOT NULL,
  `phone_number` varchar(200) COLLATE utf8_bin NOT NULL,
  `phone_activated` int(11) NOT NULL,
  `pin_sent` int(4) NOT NULL,
  `total_code_sent` int(11) NOT NULL,
  `client_password` text COLLATE utf8_bin NOT NULL,
  `client_balance` float NOT NULL DEFAULT '0' COMMENT 'balance in $'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `aswt_clients`
--

INSERT INTO `aswt_clients` (`id`, `social_id`, `client_email`, `social_type`, `register_date`, `device_udid`, `device_token`, `user_name`, `full_name`, `phone_number`, `phone_activated`, `pin_sent`, `total_code_sent`, `client_password`, `client_balance`) VALUES
(1, '12', NULL, 1, 1578774166, 'Android', NULL, NULL, '', '', 0, 0, 0, '', 0),
(2, '123', NULL, 1, 1578774372, NULL, NULL, NULL, '', '', 0, 0, 0, '', 0),
(3, '1234', '', 1, 1579034354, NULL, NULL, NULL, '', '', 0, 0, 0, '', 0),
(4, '1', NULL, 1, 1579341685, NULL, NULL, NULL, '', '', 0, 0, 0, '', 0),
(5, '1', NULL, 0, 1579341703, NULL, NULL, NULL, '', '', 0, 0, 0, '', 0),
(6, '16999', NULL, 1, 1579341724, NULL, NULL, NULL, '', '', 0, 0, 0, '', 0),
(7, '10218119843641457', 'thescream_ad@yahoo.com', 1, 1579369334, NULL, '', 'Ahmad Z. Qasem', '', '', 0, 0, 0, '', 0),
(8, '115303383131332042969', 'qasem.android@gmail.com', 0, 1579554081, NULL, '', 'Ahmad qasem', '', '', 0, 0, 0, '', 0),
(9, '11430603175779815673522', 'mohdsh85@gmail.com2', 1, 1580164198, NULL, '', 'mohammad al shareif', '', '', 0, 0, 0, '', 0),
(10, '114306031757798156735hh', NULL, 2, 1580198220, NULL, NULL, NULL, '', '', 0, 0, 0, '', 0),
(11, '115303383131332042969', 'qasem.android@gmail.com', 2, 1580244242, 'Android', 'd0fnQWIq7L4:APA91bEqH9ouYlBPt_dFA1yUhSm5Oz44XDmVarI4fyq6iqA3aqJrvQLoJyhKCbGyyRmaHQssWCNARVs2y7ULgvU34rxXbyBOle9_OjTvEf4gnFIUQOcjJLDD4iP4wfUHcoPC4fs6DUM9', 'Ahmad qasem', '', '', 0, 0, 0, '', 0),
(12, '12345', NULL, 1, 1580499268, NULL, NULL, NULL, '', '', 0, 0, 0, '', 0),
(13, '114306031757798156735', 'mohdsh85@gmail.com', 2, 1580505896, 'Android', 'c_utnaOhMCc:APA91bHzoyxv6N-hgj7NIQcaaxe871-trddRwQomkBDpEH9EtC4FQD0rWi01GPUVgER0O2R3V6qvdHYChIpoq7c7xX39vuU3mV-okyJv4Hxb0jmvj3ISYBOpsWJUHQYn5rSJGxKHBftO', 'mohammad al shareif', '', '', 0, 0, 0, '', 0),
(14, '10157708412520491', 'mohdsh85@gmail.com', 1, 1580565355, 'Android', 'c_utnaOhMCc:APA91bHzoyxv6N-hgj7NIQcaaxe871-trddRwQomkBDpEH9EtC4FQD0rWi01GPUVgER0O2R3V6qvdHYChIpoq7c7xX39vuU3mV-okyJv4Hxb0jmvj3ISYBOpsWJUHQYn5rSJGxKHBftO', 'Mohammed Al-shareif', '', '', 0, 0, 0, '', 0),
(15, '114306031757798156735', NULL, 1, 1581540454, 'Android', NULL, NULL, '', '', 0, 0, 0, '', 0),
(20, '', '', 0, 1601757581, 'demo', 'demo', 'shareif', 'shareif', '962797255009', 1, 4205, 0, '', 0),
(23, '', '', 0, 1601838561, 'demo', 'demo', 'shareif', 'shareif', '962797255002', 0, 4848, 0, '', 0),
(25, '', '', 0, 1601839239, 'demo', 'demo', 'shareif', 'shareif', '496279725500', 0, 365, 0, '', 0),
(29, '', '', 0, 1601929372, 'demo', 'demo', 'shareif', 'shareif', '962797255001', 0, 3391, 0, '14c89ce40a2de5f8d96868c9ce990a33', 0),
(33, '', '', 0, 1608245359, 'demo', 'demo', 'shareif', 'shareif', '972569151611', 1, 6755, 0, '$2y$10$nDOG8pPJOY6Pekp/l7iXxeimtt/3hCmnR3xt8HMXSlspkmrs9kXgy', 20.7);

-- --------------------------------------------------------

--
-- Table structure for table `aswt_client_logs`
--

CREATE TABLE `aswt_client_logs` (
  `id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `datetime` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `aswt_client_service_balances`
--

CREATE TABLE `aswt_client_service_balances` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `balance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `aswt_client_subscribers`
--

CREATE TABLE `aswt_client_subscribers` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `subscription_date` int(11) NOT NULL,
  `expiry_date` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `aswt_client_subscribers`
--

INSERT INTO `aswt_client_subscribers` (`id`, `client_id`, `package_id`, `subscription_date`, `expiry_date`, `status`) VALUES
(39, 33, 4, 1609798076, 1612304396, 0);

-- --------------------------------------------------------

--
-- Table structure for table `aswt_client_transaction_logs`
--

CREATE TABLE `aswt_client_transaction_logs` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `date` text NOT NULL,
  `balance` float NOT NULL,
  `package_id` int(11) NOT NULL DEFAULT '0',
  `old_balance` float NOT NULL,
  `discount_rate` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aswt_client_transaction_logs`
--

INSERT INTO `aswt_client_transaction_logs` (`id`, `client_id`, `amount`, `date`, `balance`, `package_id`, `old_balance`, `discount_rate`) VALUES
(2, 33, -7, '1609797990', 136, 4, 143, 0),
(3, 33, -7, '1609798076', 129, 4, 136, 0),
(4, 33, -11.4, '1609799197', 94.8, 0, 106.2, 0.05),
(5, 33, -11.4, '1609799215', 83.4, 0, 94.8, 0.05),
(6, 33, -11.4, '1609799232', 72, 0, 83.4, 0.05),
(7, 33, -11.4, '1609799287', 60.6, 0, 72, 0.05),
(8, 33, -11.4, '1609799375', 49.2, 0, 60.6, 0.05),
(9, 33, -9.5, '1609800673', 39.7, 0, 49.2, 0.05),
(10, 33, -9.5, '1609800696', 30.2, 0, 39.7, 0.05),
(11, 33, -9.5, '1609800716', 20.7, 0, 30.2, 0.05);

-- --------------------------------------------------------

--
-- Table structure for table `aswt_packages`
--

CREATE TABLE `aswt_packages` (
  `package_id` int(11) NOT NULL,
  `package_ar` varchar(50) COLLATE utf8_bin NOT NULL,
  `package_en` varchar(50) COLLATE utf8_bin NOT NULL,
  `desc_en` varchar(300) COLLATE utf8_bin NOT NULL,
  `desc_ar` varchar(300) COLLATE utf8_bin NOT NULL,
  `service_list` text COLLATE utf8_bin NOT NULL,
  `package_price` text COLLATE utf8_bin NOT NULL COMMENT 'price $',
  `is_offer` int(1) NOT NULL DEFAULT '0',
  `package_period_days` int(20) NOT NULL,
  `package_period_label_en` varchar(200) COLLATE utf8_bin NOT NULL,
  `package_period_label_ar` varchar(200) COLLATE utf8_bin NOT NULL,
  `on_off` int(11) NOT NULL COMMENT '0 is off, 1 is on',
  `default_package` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `aswt_packages`
--

INSERT INTO `aswt_packages` (`package_id`, `package_ar`, `package_en`, `desc_en`, `desc_ar`, `service_list`, `package_price`, `is_offer`, `package_period_days`, `package_period_label_en`, `package_period_label_ar`, `on_off`, `default_package`) VALUES
(4, 'default_package', 'default_package', 'default_package', 'default_package', '[*]', '7', 0, 30, 'default_package', 'default_package', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `aswt_package_details`
--

CREATE TABLE `aswt_package_details` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `total_times` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `aswt_package_details`
--

INSERT INTO `aswt_package_details` (`id`, `package_id`, `service_id`, `total_times`) VALUES
(6, 4, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `aswt_pp_agrement`
--

CREATE TABLE `aswt_pp_agrement` (
  `id` int(11) NOT NULL,
  `privacy_policy_ar` text COLLATE utf8_bin NOT NULL,
  `privacy_policy_en` text COLLATE utf8_bin NOT NULL,
  `user_agrement_ar` text COLLATE utf8_bin NOT NULL,
  `user_agrement_en` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `aswt_pp_agrement`
--

INSERT INTO `aswt_pp_agrement` (`id`, `privacy_policy_ar`, `privacy_policy_en`, `user_agrement_ar`, `user_agrement_en`) VALUES
(1, 'f', 'g', 'hgf', 'hfg');

-- --------------------------------------------------------

--
-- Table structure for table `aswt_providers`
--

CREATE TABLE `aswt_providers` (
  `provider_id` int(11) NOT NULL,
  `provider_name_en` varchar(200) COLLATE utf8_bin NOT NULL,
  `provider_name_ar` varchar(200) COLLATE utf8_bin NOT NULL,
  `image` text COLLATE utf8_bin NOT NULL,
  `provider_email` varchar(350) COLLATE utf8_bin NOT NULL,
  `provider_password` varchar(250) COLLATE utf8_bin NOT NULL,
  `discount_share` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `aswt_providers`
--

INSERT INTO `aswt_providers` (`provider_id`, `provider_name_en`, `provider_name_ar`, `image`, `provider_email`, `provider_password`, `discount_share`) VALUES
(1, 'Total', 'توتال', 'http://vestnikkavkaza.net/upload2/2015-07-29/0bb65c033a589b0ced99047321bc4d8e.jpg', 'mohdsh85@gmail.com', '14e1b600b1fd579f47433b88e8d85291', 0),
(2, 'Abu Jbarah', 'أبو جباره', 'http://localhost/scan_park/assets/providerImages/', 'mohdsh85@hotmail.com', '14e1b600b1fd579f47433b88e8d85291', 0);

-- --------------------------------------------------------

--
-- Table structure for table `aswt_provider_logs`
--

CREATE TABLE `aswt_provider_logs` (
  `id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `paid` int(11) NOT NULL,
  `datetime` int(11) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `aswt_provider_logs`
--

INSERT INTO `aswt_provider_logs` (`id`, `provider_id`, `client_id`, `service_id`, `paid`, `datetime`, `amount`) VALUES
(1, 2, 33, 0, 0, 1609800696, -10),
(2, 2, 33, 0, 0, 1609800716, 10);

-- --------------------------------------------------------

--
-- Table structure for table `aswt_push_messages`
--

CREATE TABLE `aswt_push_messages` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `text` varchar(250) COLLATE utf8_bin NOT NULL,
  `parsed` int(11) NOT NULL,
  `datetime` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `aswt_push_messages`
--

INSERT INTO `aswt_push_messages` (`id`, `client_id`, `text`, `parsed`, `datetime`) VALUES
(1, 14, 'sendQr', 1, 1580674481),
(2, 14, 'sendQr', 1, 1580674846),
(3, 14, 'sendQr', 1, 1580675310),
(4, 14, 'sendQr', 1, 1580675812),
(5, 14, 'cancelSubscription', 1, 1580675894),
(6, 11, 'billingProcessApp', 1, 1580675965),
(7, 14, 'sendQr', 1, 1580676606),
(8, 11, 'sendQr', 1, 1580676609),
(9, 11, 'sendQr', 1, 1580677424),
(10, 11, 'hh', 1, 1580677461),
(11, 14, 'cancelSubscription', 1, 1580681113),
(12, 11, '', 1, 1580714844),
(13, 13, '', 1, 1580715854),
(14, 11, 'cancelSubscription', 1, 1580765700),
(15, 11, 'billingProcessApp', 1, 1580766149),
(16, 11, 'cancelSubscription', 1, 1580766259),
(17, 11, 'billingProcessApp', 1, 1580766404),
(18, 11, 'cancelSubscription', 1, 1580768569),
(19, 14, 'billingProcessApp', 1, 1580769125),
(20, 13, 'cancelSubscription', 1, 1580769422),
(21, 13, 'billingProcessApp', 1, 1580823985),
(22, 13, 'sendQr', 1, 1580823999),
(23, 13, 'sendQr', 1, 1580827829),
(24, 13, 'cancelSubscription', 1, 1580827865),
(25, 13, 'billingProcessApp', 1, 1580827913),
(26, 14, 'cancelSubscription', 1, 1580827957),
(27, 14, 'billingProcessApp', 1, 1580837349),
(28, 13, '', 1, 1580885156),
(29, 14, 'sendQr', 1, 1580894398),
(30, 14, 'sendQr', 1, 1580895874),
(31, 13, '', 1, 1580972856),
(32, 11, 'sendQr', 1, 1580989185),
(33, 14, 'sendQr', 1, 1581030213),
(34, 14, 'cancelSubscription', 1, 1581030303),
(35, 14, 'billingProcessApp', 1, 1581030580),
(36, 14, 'sendQr', 1, 1581030869),
(37, 14, 'cancelSubscription', 1, 1581030922),
(38, 14, 'billingProcessApp', 1, 1581030978),
(39, 13, '', 1, 1581148692),
(40, 11, 'billingProcessApp', 1, 1581191466),
(41, 13, '', 1, 1581235238),
(42, 14, 'sendQr', 1, 1581236693),
(43, 14, 'sendQr', 1, 1581236818),
(44, 14, 'sendQr', 1, 1581451952),
(45, 11, 'cancelSubscription', 1, 1581455235),
(46, 14, 'cancelSubscription', 1, 1581459294),
(47, 14, 'billingProcessApp', 1, 1581459345),
(48, 14, 'sendQr', 1, 1581534507),
(49, 14, 'sendQr', 1, 1581534522),
(50, 14, 'sendQr', 1, 1581534569),
(51, 14, 'sendQr', 1, 1581534595),
(52, 14, 'sendQr', 1, 1581534613),
(53, 14, 'sendQr', 1, 1581535067),
(54, 14, 'sendQr', 1, 1581535122),
(55, 11, 'billingProcessApp', 1, 1581535498),
(56, 11, 'sendQr', 1, 1581535569),
(57, 11, 'sendQr', 1, 1581535677),
(58, 14, 'cancelSubscription', 1, 1581535977),
(59, 11, 'sendQr', 1, 1581535980),
(60, 14, 'billingProcessApp', 1, 1581540335),
(61, 14, 'sendQr', 1, 1581540347),
(62, 14, 'sendQr', 1, 1581540358),
(63, 14, 'sendQr', 1, 1581540380),
(64, 14, 'sendQr', 1, 1581541297),
(65, 14, 'sendQr', 1, 1581541303),
(66, 14, 'cancelSubscription', 1, 1581630207),
(67, 14, 'billingProcessApp', 1, 1581680709),
(68, 13, 'sendQr', 1, 1582013658),
(69, 11, 'cancelSubscription', 1, 1582368779),
(70, 13, 'cancelSubscription', 1, 1583320549),
(71, 13, 'billingProcessApp', 1, 1583320596);

-- --------------------------------------------------------

--
-- Table structure for table `aswt_services`
--

CREATE TABLE `aswt_services` (
  `service_id` int(11) NOT NULL,
  `ar_name` varchar(200) COLLATE utf8_bin NOT NULL,
  `en_name` varchar(200) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `aswt_services`
--

INSERT INTO `aswt_services` (`service_id`, `ar_name`, `en_name`) VALUES
(3, 'all', 'all');

-- --------------------------------------------------------

--
-- Table structure for table `aswt_service_provider`
--

CREATE TABLE `aswt_service_provider` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `aswt_service_provider`
--

INSERT INTO `aswt_service_provider` (`id`, `service_id`, `provider_id`) VALUES
(5, 3, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aswt_billing_token`
--
ALTER TABLE `aswt_billing_token`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `token` (`token`);

--
-- Indexes for table `aswt_clients`
--
ALTER TABLE `aswt_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_id` (`social_id`);

--
-- Indexes for table `aswt_client_logs`
--
ALTER TABLE `aswt_client_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provider_id` (`provider_id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `provcllogscl` (`client_id`);

--
-- Indexes for table `aswt_client_service_balances`
--
ALTER TABLE `aswt_client_service_balances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `aswt_client_subscribers`
--
ALTER TABLE `aswt_client_subscribers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `aswt_client_transaction_logs`
--
ALTER TABLE `aswt_client_transaction_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clientIndex` (`client_id`);

--
-- Indexes for table `aswt_packages`
--
ALTER TABLE `aswt_packages`
  ADD PRIMARY KEY (`package_id`);

--
-- Indexes for table `aswt_package_details`
--
ALTER TABLE `aswt_package_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `aswt_pp_agrement`
--
ALTER TABLE `aswt_pp_agrement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aswt_providers`
--
ALTER TABLE `aswt_providers`
  ADD PRIMARY KEY (`provider_id`);

--
-- Indexes for table `aswt_provider_logs`
--
ALTER TABLE `aswt_provider_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provider_id` (`provider_id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `provrelservcli` (`client_id`);

--
-- Indexes for table `aswt_push_messages`
--
ALTER TABLE `aswt_push_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `aswt_services`
--
ALTER TABLE `aswt_services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `aswt_service_provider`
--
ALTER TABLE `aswt_service_provider`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `provider_id` (`provider_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aswt_billing_token`
--
ALTER TABLE `aswt_billing_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `aswt_clients`
--
ALTER TABLE `aswt_clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `aswt_client_logs`
--
ALTER TABLE `aswt_client_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `aswt_client_service_balances`
--
ALTER TABLE `aswt_client_service_balances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `aswt_client_subscribers`
--
ALTER TABLE `aswt_client_subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `aswt_client_transaction_logs`
--
ALTER TABLE `aswt_client_transaction_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `aswt_packages`
--
ALTER TABLE `aswt_packages`
  MODIFY `package_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `aswt_package_details`
--
ALTER TABLE `aswt_package_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `aswt_pp_agrement`
--
ALTER TABLE `aswt_pp_agrement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `aswt_providers`
--
ALTER TABLE `aswt_providers`
  MODIFY `provider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `aswt_provider_logs`
--
ALTER TABLE `aswt_provider_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `aswt_push_messages`
--
ALTER TABLE `aswt_push_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `aswt_services`
--
ALTER TABLE `aswt_services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `aswt_service_provider`
--
ALTER TABLE `aswt_service_provider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `aswt_client_logs`
--
ALTER TABLE `aswt_client_logs`
  ADD CONSTRAINT `provcllogs` FOREIGN KEY (`provider_id`) REFERENCES `aswt_providers` (`provider_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `provcllogscl` FOREIGN KEY (`client_id`) REFERENCES `aswt_clients` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `provcllogsclserv` FOREIGN KEY (`service_id`) REFERENCES `aswt_services` (`service_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `aswt_client_service_balances`
--
ALTER TABLE `aswt_client_service_balances`
  ADD CONSTRAINT `client_rel` FOREIGN KEY (`client_id`) REFERENCES `aswt_clients` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `client_rel_2` FOREIGN KEY (`service_id`) REFERENCES `aswt_services` (`service_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `aswt_client_subscribers`
--
ALTER TABLE `aswt_client_subscribers`
  ADD CONSTRAINT `clientRel` FOREIGN KEY (`client_id`) REFERENCES `aswt_clients` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `packageRel` FOREIGN KEY (`package_id`) REFERENCES `aswt_packages` (`package_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `aswt_client_transaction_logs`
--
ALTER TABLE `aswt_client_transaction_logs`
  ADD CONSTRAINT `clientindexlogs` FOREIGN KEY (`client_id`) REFERENCES `aswt_clients` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `aswt_package_details`
--
ALTER TABLE `aswt_package_details`
  ADD CONSTRAINT `packagel` FOREIGN KEY (`package_id`) REFERENCES `aswt_packages` (`package_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `packagelService` FOREIGN KEY (`service_id`) REFERENCES `aswt_services` (`service_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `aswt_provider_logs`
--
ALTER TABLE `aswt_provider_logs`
  ADD CONSTRAINT `provrel` FOREIGN KEY (`provider_id`) REFERENCES `aswt_providers` (`provider_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `provrelservcli` FOREIGN KEY (`client_id`) REFERENCES `aswt_clients` (`id`);

--
-- Constraints for table `aswt_service_provider`
--
ALTER TABLE `aswt_service_provider`
  ADD CONSTRAINT `providerRel` FOREIGN KEY (`provider_id`) REFERENCES `aswt_providers` (`provider_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `serviceRel` FOREIGN KEY (`service_id`) REFERENCES `aswt_services` (`service_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
