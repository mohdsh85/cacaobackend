<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.4/cropper.min.css'>
</head>
<body>
<div >
    
    <div id="fh5co-page">
		<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
		<aside id="fh5co-aside" role="complementary" class="border js-fullheight">

			
			<nav id="fh5co-main-menu" role="navigation">
                            <?=nav_menu()?>
                                
			</nav>

                    <?=                    nav_menu_footer()?>

		</aside>

		<div id="fh5co-main">
<div class="container uploadingArea">
  <?=$info['intro_message']?> <a href="<?= config('base_url').'index.php/welcome/logout'?>"><?=lang('logout')?></a>
	        <div class="box2Input">
                    <div class="labelInputH">  <?=lang('category')?>:</div>
                 <div class="labelInputI">        <?=categoryList()?></div>
	</div>
          <div class="box2Input">
              <div class="labelInputH"> <?=lang('image_caption')?>:</div>
                        <div class="labelInputI"> <input type="text" id="image_caption"></div>
	</div>
<main class="page">

	<!-- input file -->
<div class="box2Input">
    <div class="labelInputH"> <?=lang('image')?>:</div>
	<div class="labelInputI"><div class="box">
	       <input type="file" id="file-input">
	</div>
        </div>
</div>
	<!-- leftbox -->
	<div class="box-2">
		<div class="result"></div>
	</div>
	<!--rightbox-->
	<div class="box-2 img-result hide">
		<!-- result of crop -->
		<img class="cropped" src="" alt="">
	</div>
	<!-- input file -->
	<div class="box">
		<div class="options hide">
			<label> Width</label>
			<input type="number" class="img-w" value="300" min="300" max="600" />
		</div>
		<!-- save btn -->
		<button class="btn save hide">Save</button>
		<!-- download btn -->
		<a href="" class="btn download hide">Download</a>
	</div>
</main>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.js'></script>

  

    <script  src="<?= config('assets_path')?>js/index.js"></script>


</div>
			
	


    

</div>

</body>
