<head>
<style>
    .mainBody
    {
        width: 100%;
        
        border: 0px solid;
        margin-top: 15px;
    }
    .imgClass
    {
	    max-width: 40%;
    width: 40%;
    float: left;
    text-align: center;
        
    }
    .commentArea
    {
        width:59%;
    float:left;
    }
        .headerBox
    {
        width:100%;
        font-family: arial;
        font-size: 26px;
		text-align:center;
    }
.widget .panel-body { padding:10px; }
.widget .list-group { margin-bottom: 0; }
.widget .panel-title { display:inline }
.widget .label-info { float: right;font-size:14px }
.widget li.list-group-item {border-radius: 0;border: 0;border-top: 1px solid #ddd;}
.widget li.list-group-item:hover { background-color: rgba(86,61,124,.1); }
.widget .mic-info {color: black;
    font-size: 14px;
    text-align: left;
    font-style: italic; }
.widget .action { margin-top:5px; }
.widget .comment-text { font-size: 14px;    line-height: 1.5;    text-align: justify; }
.widget .btn-block { border-top-left-radius:0px;border-top-right-radius:0px; }
.swal-wide
{
    height: 580px;
}
    </style>
<link href="<?= config('assets_path')?>css/bootstrap_min.css" rel="stylesheet" id="bootstrap-css">

<script src="<?= config('assets_path')?>js/js.js"></script>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?= config('assets_path')?>libs/prettify/prettify.js"></script>

<script type="text/javascript" src="<?= config('assets_path')?>jquery.slimscroll.js"></script>

	<meta property="og:title" content="<?=lang('website_title')?>"/>
	<meta property="og:image" content="<?=$image?>"/>
	<meta property="og:url" content="<?=$url?>"/>
	<meta property="og:site_name" content="<?=lang('website_title')?>"/>
        <meta property="og:description" content="<?=$description?>"/>
        <meta property="og:width" content="600"/>
        <meta property="og:height" content="600"/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="<?=$image?>" />
	<meta name="twitter:url" content="<?=$url?>" />
	<meta name="twitter:card" content="<?=lang('website_title')?>" />
</head>
<body>
<div >
    
    <div id="fh5co-page">
		<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
		<aside id="fh5co-aside" role="complementary" class="border js-fullheight">

			
			<nav id="fh5co-main-menu" role="navigation">
                            <?=nav_menu()?>
			</nav>

			 <?=                    nav_menu_footer()?>

		</aside>

		<div id="fh5co-main">
			<div class="fh5co-gallery">
                            <?=$description?>
                            <div >
    <div class="mainBody">
                <div class="imgClass"><img src="<?=$image?>" /></div>

 <div class="commentArea">
<!------ Include the above in your HEAD tag ---------->
<div class="examples">
    <div >
      <div id="testDiv">
<div class="container">
    <div class="row">
        <div class="panel panel-default widget">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-comment"></span>
                <h3 class="panel-title">
                    Recent Comments</h3>
                <span class="label label-info" id="totalComments">
                    <?=$total_comments?></span>
            </div>
            <div class="panel-body">
                <ul class="list-group" id="<?= secureToken($imageId)?>">
                    <?=buildComments($comments,$imageId)?>
 
                </ul>
                <?php
                    if($total_comments> config('comment_per_page'))
                    {
                ?>
                <a id="loadButton" href="javascript:void(0)" onclick="loadMoreComments('<?=secureToken($imageId)?>','<?=$imageId?>')" class="btn btn-primary btn-sm btn-block" role="button">
                        <span class="glyphicon glyphicon-refresh"></span> <?=lang('more')?></a>
                        <?php
                    }
                        ?>
                <?=commentBox($imageId,$total_comments)?>
                    
            </div>
        </div>
    </div>
</div>
          
</div>
      </div>
    
    </div>
		
		</div>
    </div>                            <div >
                            <?=$client_images?>
                            </div>
		</div>
    </div>
                    
</body>
<input style="display:none" type="text" name="totalComments" id="totalComments" value="<?=$total_comments?>" />
<input style="display:none"  type="text" name="totalPages" id="totalPages" value="<?=ceil($total_comments/config('comment_per_page_load_more'))?>" />
<input style="display:none"  type="text" name="currentPage" id="currentPage" value="0" />


<script type="text/javascript">
//$('#testDiv').slimscroll({
//  height: 'auto'
//});
</script>
