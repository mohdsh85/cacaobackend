<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
	<!-- jQuery -->
	<script src="<?= config('assets_path')?>js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?= config('assets_path')?>js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?= config('assets_path')?>js/bootstrap.min.js"></script>
	<!-- Carousel -->
	<script src="<?= config('assets_path')?>js/owl.carousel.min.js"></script>
	<!-- Stellar -->
	<script src="<?= config('assets_path')?>js/jquery.stellar.min.js"></script>
	<!-- Waypoints -->
	<script src="<?= config('assets_path')?>js/jquery.waypoints.min.js"></script>
	<!-- Counters -->
	<script src="<?= config('assets_path')?>js/jquery.countTo.js"></script>
	
	
	<!-- MAIN JS -->
	<script src="<?= config('assets_path')?>js/main.js"></script>
        <script src="<?= config('assets_path')?>js/swalFile.js"></script>
<!--        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>-->
   