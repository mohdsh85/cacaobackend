<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8">
        <meta name="description" content="<?= lang('meta_desription')?>"/>
        <meta name="keywords" content="<?= lang('meta_desription')?>"/>
        <title><?= lang('website_title')?></title>
          	<!-- Facebook and Twitter integration -->

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	
	<!-- Animate.css -->
        <link rel="stylesheet" href="<?= config('assets_path')?>css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?= config('assets_path')?>css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?= config('assets_path')?>css/bootstrap.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="<?= config('assets_path')?>css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?= config('assets_path')?>css/owl.theme.default.min.css">
	<!-- Theme style  -->
	<link rel="stylesheet" href="<?= config('assets_path')?>css/style.css">

	<!-- Modernizr JS -->
	<script src="<?= config('assets_path')?>js/modernizr-2.6.2.min.js"></script>
        <script src="<?= config('assets_path')?>js/config.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="<?= config('assets_path')?>js/respond.min.js"></script>
	<![endif]-->
        
        </head>