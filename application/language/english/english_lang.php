<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang['error_found_desc']='الرجاء إاعدة الدخول لحسابك';
$lang['page_one_en']='Your payment gateway that will always provide you with good discount  on your must-nuy daily items ';
$lang['page_one_ar']='منصة الدفع التي ستزودك بخصومات عديدة بشكل يومي';
$lang['page_two_en']='You can subscribe for free or for minimum fee, either way..you are winning';
$lang['page_two_ar']='يمكنك الاشتراك بتكلفة بسيطه و الاستفادة من عروض غير منتهية';
$lang['verify_number_en']='Verify your number';
$lang['verify_number_ar']='يرجى تاكيد رقمك ';
$lang['verify_number_msg_en']='Enter the pin code you ever recevied on your mobile phone to continue';
$lang['verify_number_msg_ar']='الرجاء إدخال رمز التفعيل قبل الاستمرار في عملية التسجيل';
$lang['resend_code_en']='Resend Code ';
$lang['resend_code_ar']='إعادة إرسال الرمز ';
$lang['phone_registred_en']='Mobile number already registered and activated ';
$lang['phone_registred_ar']='رقم الهاتف المدخل مسجل من قبل ';
$lang['error_happened_en']='Error Happened';
$lang['error_happened_ar']='حصل خطاء ما ';
$lang['activation_pin_sent_en']='Pin Sent';
$lang['activation_pin_sent_ar']='تم إرسال الرمز';
$lang['exceed_limit_ar']='تم تجاوز العدد المسموح به لارسال رمز التفعيل';
$lang['exceed_limit_en']='Sending Pin Limit Exceed';
$lang['verification_failed_en']='Verification Failed';
$lang['verification_failed_ar']=' فشلت العملية';
$lang['phone_number_activated_en']='Phone number Activated';
$lang['phone_number_activated_ar']=' تمت عملية التفعيل بنجاح';
$lang['match_password_err_en']='Password dosent match';
$lang['match_password_err_ar']='كلمة السر غير متطابقة';
$lang['password_set_en']='Password Set';
$lang['password_set_ar']='لقد تم تحديد كلمة السر';
$lang['missing_phone_password_en']='Phone number and password required';
$lang['missing_phone_password_ar']='يجب إدخال رقم الهاتف و كلمة المرور';
$lang['erro_phone_or_password_en']='Error in phone number or password';
$lang['erro_phone_or_password_ar']='خطاء في رقم الهاتف او كلمة السر';
$lang['already_subscribed']='Already Active Subscriber!!';
$lang['already_subscribed_ar']='أنت مشترك فعال';
$lang['payment_1']='Cash';
$lang['payment_2']='Visa';
$lang['payment_3']='Wallet';
$lang['insufficiant_balance_en']='Insufficiant Balance';
$lang['insufficiant_balance_ar']='لا يوجد رصيد كافي لاتمام العملية';
$lang['default_package_not_defined_en']='default_package_not_defined';
$lang['default_package_not_defined_ar']='default_package_not_defined';
$lang['subscription_done_ar']='تم الاشتراك بنجاح';
$lang['subscription_done_en']='subscription_done_en';
$lang['authintication_erro_ar']='Try to Logout and login Again Again, Authintication Error.';
$lang['authintication_erro_en']='Try to Logout and login Again Again, Authintication Error.';
$lang['transaction_sent_to_client_to_pay_ar']='لقد تم إرسال طلب الدفع';
$lang['transaction_sent_to_client_to_pay_en']='transaction_sent_to_client_to_pay_en';
$lang['provider_id_error_ar']='provider_id_error_ar';
$lang['provider_id_error_en']='provider_id_error_ar';
$lang['status_changed_2_ar']='status_changed_2_ar';
$lang['status_changed_2_ar']='status_changed_2_ar';
$lang['status_changed_3_ar']='status_changed_3_ar';
$lang['status_changed_3_ar']='status_changed_3_ar';
$lang['status_changed_4_ar']='status_changed_4_ar';
$lang['status_changed_4_ar']='status_changed_4_ar';


