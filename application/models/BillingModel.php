<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BillingModel
 *
 * @author shareifhome
 */
class BillingModel extends CI_Model {
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    
    public function getExpired()
    {
        $this->db->where('expiry_date <',time());
        $q=$this->db->get('client_subscribers')->result();
        return $q;
    }
    public function getVisaInfo($client_id)
    {
        $this->db->where('client_id',$client_id);
        $q=$this->db->get('cc_info')->result_array();
        return $q;
    }
    public function getBillingToken($client_id)
    {
        $this->db->where('client_id',$client_id);
        $q=$this->db->get('cc_info')->result_array();
        return $q; 
    }
}
