<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApiModel
 *
 * @author shareifhome
 */
class ApiModel extends CI_Model{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    public function privacyPolicyAgrement()
    {
         $q=$this->db->get('pp_agrement')->result();
         return $q;
    }
    /*
     * packages is collection of services 
     */
    public function getPackages($package_id='',$onOff=false){
        //servcei 0 is for arabic, service 1 is for en
        $services=$this->services();
        $package_details=$this->package_details();
        if($package_id>0)
            $this->db->where('package_id',$package_id);
        
        if($onOff)
            $this->db->where('on_off',1);//incase to show active packages for client only
        $q=$this->db->get('packages')->result();
       
        if($q)
        {
            foreach($q as $loop)
            {
                //get pacages under sertvice
                $packageServcies= json_decode($loop->service_list,true);
                //loop on services to get names
                $nameAr=$nameEn=$a=array();
                for($i=0;$i<sizeof($packageServcies);$i++)
                {
                    $times=(isset($package_details[$loop->package_id.'_'.$packageServcies[$i]])?$package_details[$loop->package_id.'_'.$packageServcies[$i]]:0);
                    $nameAr[]=$services[0][$packageServcies[$i]].'('.$times.')';
                    $nameEn[]=$services[1][$packageServcies[$i]].'('.$times.')';
                  //  $a[$packageServcies[$i]]=array($services[0][$packageServcies[$i]],$services[1][$packageServcies[$i]]);
                  //  $loop->serviceIdLabels=$a;
                    $loop->{'service_'.$packageServcies[$i]}=$times;//list times for services
                }
                $loop->servicesArLabels=implode('، ',$nameAr);
                $loop->servicesEnLabels=implode(', ',$nameEn);
            }
        }
        return $q;
    }
    /*
     * return ba;lances for servics
     */
    public function buildBalances($client_id,$service_ids)
    {
        $services=$this->services();
        $this->db->where('client_id',$client_id);
        $q=$this->db->get('client_service_balances')->result_array();
        $z=array();
        if($q)
        {
            foreach($q as $loop)
            {
                $z[$loop['service_id']]=$loop['balance'];
            }
        }
       // print_r($q);
        $a=array();
        if($service_ids)
        {
            $service_ids=json_decode($service_ids,true);
            foreach($service_ids as $key=>$value)
            {
                $nameAr[]=($services[0][$value]).' => '.$z[$value];
                $nameEn[]=($services[1][$value]).' => '.$z[$value];
            }
           $a[0]=implode('، ',$nameAr);
           $a[1]=implode(', ',$nameEn);
        }
        return $a;
    }
    /*
     * services its should be customized as well as needed
     */
    public function services($serviceId=0){
        
        $aAr=$aEn=array();
        if($serviceId)
            $this->db->where('service_id',$serviceId);
        $q=$this->db->get('services')->result();
        if($q)
        {
            foreach($q as $loop)
            {
                $aAr[$loop->service_id]=$loop->ar_name;
                $aEn[$loop->service_id]=$loop->en_name;
            }
        }
        return [$aAr,$aEn];
    }
    
     public function providers($providerId=0){
        
        $aAr=$aEn=array();
        if($providerId)
            $this->db->where('provider_id',$providerId);
        $q=$this->db->get('providers')->result();
        if($q)
        {
            foreach($q as $loop)
            {
                $aAr[$loop->provider_id]=$loop->provider_name_ar;
                $aEn[$loop->provider_id]=$loop->provider_name_en;
            }
        }
        return [$aAr,$aEn];
    }
    
         public function providersImages($providerId=0){
        
        $aAr=$aEn=array();
        if($providerId)
            $this->db->where('provider_id',$providerId);
        $q=$this->db->get('providers')->result();
        if($q)
        {
            foreach($q as $loop)
            {
                $aAr[$loop->provider_id]=array($loop->provider_name_ar,$loop->image);
                $aEn[$loop->provider_id]=array($loop->provider_name_en,$loop->image);
            }
        }
        return [$aAr,$aEn];
    }
    public function getServiceProvider()
    {
        $aAr=array();
        $q=$this->db->get('service_provider')->result();
        if($q)
        {
            foreach($q as $loop)
            {
                $aAr[$loop->service_id][]=$loop->provider_id;
                
            }
        }
        return $aAr;
    }
    /*
     * getClientLogs
     */
    public function getClientLogs($client_id)
    {
        $serviceList=$this->services();
        $prividers=$this->providers();
        $this->db->where('client_id',$client_id);
        $this->db->order_by('datetime','desc');
        $this->db->limit(50);
        $q=$this->db->get('client_logs')->result();
        if($q)
        {
            $array=$arrayRetuen=array();
            $j=count($q);
            foreach($q as $loop)
            {
                $array['number']=$j;
                $j--;
                $array['date']=date('d M Y',$loop->datetime);
                $array['time']=date('H:i a',$loop->datetime);
                $array['desc_ar']=$prividers[0][$loop->provider_id].'،'.$serviceList[0][$loop->service_id];
                $array['desc_en']=$prividers[1][$loop->provider_id].'،'.$serviceList[1][$loop->service_id];
                unset($loop->datetime);
                $arrayRetuen[]=$array;
            }
            return $arrayRetuen;
        }
        else
        {
            return array();
        }
    }
    public function package_details(){
        
        $aAr=$list=array();
        $q=$this->db->get('package_details')->result();
        if($q)
        {
            foreach($q as $loop)
            {
                $list[$loop->package_id.'_'.$loop->service_id]=$loop->total_times;
                
            }
        }
        return $list;
    }
    /*
     * checkRegisterUser
     */
    public function checkRegisterUser($where)
    {
        $this->db->where($where);
        $this->db->select('id');
        $q=$this->db->get('clients')->row_array();
        if($q)
        {
            return $q['id'];
        }
        return 0;//new user
    }
    /*
     * cretae usert
     */
    public function createUser($user_name='',$user_email='',$user_socail_id,$type,$device_token='',$device_udid='')
    {
        $data=array(
            'social_id'=>$user_socail_id,
            'client_email'=>$user_email,
            'social_type'=>$type,
            'register_date'=>time(),
            'device_udid'=>$device_udid,
            'device_token'=>$device_token,
            'user_name'=>$user_name
        );
        $q=$this->db->insert('clients',$data);
        $client_id=$this->db->insert_id();
        return $client_id;
    }
    /*
     * return subscription data
     */
    public function getSubscriptionData($client_id)
    {
        $this->db->where('client_id',$client_id);
        $q=$this->db->get('client_subscribers')->row_array();
        if($q)
        {
            
        }
        return $q;
    }
    /*
     * check if client have getPendingBillings
     */
    public function getPendingBillings($client_id)
    {
        $this->db->where('client_id',$client_id);
        $this->db->where('parsed',0);
        $q=$this->db->get('billing_token')->row_array();
        if($q)
            return sizeof($q);
        return 0;
    }
    /*
     * checkTokenValidity
     */
    public function checkTokenValidity($token)
    {
         $this->db->where('token',$token);
        $this->db->where('parsed',0);
        $q=$this->db->get('billing_token')->row_array();
        return sizeof($q);
    }
    
    /*
     * process billing confirmation 
     * from app not web
     */
    public function processBillingApp($token,$status,$transaction_id,$client_id,$package_id)
    {
        //insert token info
        $status='failed';
        $visa=$this->visaApiTransactionId($transaction_id);
        if($visa)
        {
            $dataVisa= json_decode($visa,true);

            if($dataVisa['response_code']==100)
            {
                $amount= $dataVisa['amount'];
                $visa_trans_id=$dataVisa['transaction_id'];
                $currency=$dataVisa['currency'];
                $payment_reference=(int)$dataVisa['pt_invoice_id'];                
                $status='success';
                $array=array('token'=>$token,'package_id'=>$package_id,'client_id'=>$client_id,'parsed'=>1,'amount'=>$amount,'visa_trans_id'=>$transaction_id,'billing_status'=>$status
                        ,'datetime'=>time(),'payment_reference'=>$payment_reference,'billing_completed_visa_side'=>1,'currency'=>$currency);
            }
            else
            if($dataVisa['response_code']==4003)
            {
                $array=array('token'=>$token,'package_id'=>$package_id,'client_id'=>$client_id,'parsed'=>1,'amount'=>0,'visa_trans_id'=>$transaction_id,'billing_status'=>'failed'
                    ,'datetime'=>time(),'payment_reference'=>0,'billing_completed_visa_side'=>1,'currency'=>'err');
            }
            else
            {
                $amount= $dataVisa['amount'];
                $visa_trans_id=$dataVisa['transaction_id'];
                $currency=$dataVisa['currency'];
                $payment_reference=(int)$dataVisa['pt_invoice_id'];    
                $status='failed';
                $array=array('token'=>$token,'package_id'=>$package_id,'client_id'=>$client_id,'parsed'=>1,'amount'=>$amount,'visa_trans_id'=>$transaction_id,'billing_status'=>$status
                        ,'datetime'=>time(),'payment_reference'=>$payment_reference,'billing_completed_visa_side'=>1,'currency'=>$currency);                
            }

            $this->db->insert('billing_token',$array);
         /*
          * success billing
          */   
        if($status=='success')
        {
            //get token detials
            $this->db->where('token',$token);
            $q=$this->db->get('billing_token')->row_array();
            $package_id=$q['package_id'];
            $client_id=$q['client_id'];
            $this->db->where('package_id',$package_id);//package expiry 
            $this->db->select('package_period_days');
            $getPackagePeriod=$this->db->get('packages')->row_array()['package_period_days'];
            //echo $getPackagePeriod;
            $this->db->where('package_id',$package_id);//package servcies
            $getPackageServiceDetails=$this->db->get('package_details')->result_array();
            //push msg
            //print_r($getPackageServiceDetails);
            if($getPackageServiceDetails)
            {
                /*
                 * delete from 
                 * remove any balances from services
                 */
                 $this->db->where('client_id',$client_id);//package expiry 
                 $this->db->delete('client_service_balances');//
                 //
                 //---------------------end remove from servcies
                 //--------add to subscription table 
                $subData=array('client_id'=>$client_id,'package_id'=>$package_id,'subscription_date'=>time(),'expiry_date'=>(time()+($getPackagePeriod*24*60*60)));
                $q=$this->db->insert('client_subscribers',$subData);
                //---------end insert to subnscription
                //-----insert package srveice to service table 
                if($q)
                {
                    foreach($getPackageServiceDetails as $l)
                    {
                        $service_id= $l['service_id'];
                        $balance= $l['total_times'];
                        $serviceData=array('client_id'=>$client_id,'service_id'=>$service_id,'balance'=>$balance);
                        $this->db->insert('client_service_balances',$serviceData);
                    }                    
                }

            }
        }
        else
        {
            //push faild msg
        }
            
        }
        
        
    }



    /*
     * visaApi for visa compoany
     */
    private function visaApiTransactionId($transaction_id)
    {
        $url='https://www.paytabs.com/apiv2/verify_payment_transaction';
        // $url='https://www.paytabs.com/apiv2/verify_payment_transaction';
   
        $fields=array('merchant_email'=>'mohdsh85@gmail.com',
            'secret_key'=>'HQLxsmlEsCk2lXGsAskdVrxBbAqxcUITLurdRNSjqclLa2dKuxYpYw1a6sHjmqd7UFlLsd4TqqEAsRlXpkmnQoyWRffeFtmA8IlB',
            'transaction_id'=>$transaction_id);
        $fields_string = "";
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        $fields_string = rtrim($fields_string, '&');
        $ch = curl_init();
        $ip = $_SERVER['REMOTE_ADDR'];

        $ip_address = array(
            "REMOTE_ADDR" => $ip,
            "HTTP_X_FORWARDED_FOR" => $ip
        );
		curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HEADER, false);
          curl_setopt($ch, CURLOPT_TIMEOUT, 30);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
          curl_setopt($ch, CURLOPT_VERBOSE, true);
		/*
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $ip_address);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_REFERER, 1);
*/
        $result = curl_exec($ch);
        curl_close($ch);
      // echo $result;
        return $result;
    }




    /*
     * completet billing process
     */
    public function processBilling($token,$status,$payment_refrence)
    {
        $this->db->where(array('token'=>$token));
        $data=array('parsed'=>1,'datetime'=>time(),'payment_reference'=>$payment_refrence);
        $this->db->update('billing_token',$data);
        ///--------get data fropm visa
        $visa=$this->visaApi($payment_refrence);
        $status='failed';
        if($visa)
        {
            $dataVisa= json_decode($visa,true);
            if($dataVisa['response_code']==100)
                $status='success';
            
            $data=array('billing_status'=>$status,'visa_trans_id'=>$dataVisa['transaction_id'],'currency'=>$dataVisa['currency'],'amount'=>$dataVisa['amount'],'reference_no'=>$dataVisa['reference_no'],'billing_completed_visa_side'=>1);
            $this->db->update('billing_token',$data);
        }
        
        
      //  return;
        
        
        
        
        
        
        
        if($status=='success')
        {
            //get token detials
            $this->db->where('token',$token);
            $q=$this->db->get('billing_token')->row_array();
            $package_id=$q['package_id'];
            $client_id=$q['client_id'];
            $this->db->where('package_id',$package_id);//package expiry 
            $this->db->select('package_period_days');
            $getPackagePeriod=$this->db->get('packages')->row_array()['package_period_days'];
            //echo $getPackagePeriod;
            $this->db->where('package_id',$package_id);//package servcies
            $getPackageServiceDetails=$this->db->get('package_details')->result_array();
            //push msg
            //print_r($getPackageServiceDetails);
            if($getPackageServiceDetails)
            {
                /*
                 * delete from 
                 * remove any balances from services
                 */
                 $this->db->where('client_id',$client_id);//package expiry 
                 $this->db->delete('client_service_balances');//
                 //
                 //---------------------end remove from servcies
                 //--------add to subscription table 
                $subData=array('client_id'=>$client_id,'package_id'=>$package_id,'subscription_date'=>time(),'expiry_date'=>(time()+($getPackagePeriod*24*60*60)));
                $q=$this->db->insert('client_subscribers',$subData);
                //---------end insert to subnscription
                //-----insert package srveice to service table 
                if($q)
                {
                    foreach($getPackageServiceDetails as $l)
                    {
                        $service_id= $l['service_id'];
                        $balance= $l['total_times'];
                        $serviceData=array('client_id'=>$client_id,'service_id'=>$service_id,'balance'=>$balance);
                        $this->db->insert('client_service_balances',$serviceData);
                    }                    
                }

            }
        }
        else
        {
            //push faild msg
        }
    }
    /*
     * visaApi for visa compoany
     */
    private function visaApi($payment_reference)
    {
        $url='https://www.paytabs.com/apiv2/verify_payment';
        // $url='https://www.paytabs.com/apiv2/verify_payment_transaction';
   
        $fields=array('merchant_email'=>'mohdsh85@gmail.com',
            'secret_key'=>'HQLxsmlEsCk2lXGsAskdVrxBbAqxcUITLurdRNSjqclLa2dKuxYpYw1a6sHjmqd7UFlLsd4TqqEAsRlXpkmnQoyWRffeFtmA8IlB',
            'payment_reference'=>$payment_reference);
        $fields_string = "";
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        $fields_string = rtrim($fields_string, '&');
        $ch = curl_init();
        $ip = $_SERVER['REMOTE_ADDR'];

        $ip_address = array(
            "REMOTE_ADDR" => $ip,
            "HTTP_X_FORWARDED_FOR" => $ip
        );
		curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HEADER, false);
          curl_setopt($ch, CURLOPT_TIMEOUT, 30);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
          curl_setopt($ch, CURLOPT_VERBOSE, true);
		/*
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $ip_address);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_REFERER, 1);
*/
        $result = curl_exec($ch);
        curl_close($ch);
      // echo $result;
        return $result;
    }

    public function cancelSubscription($client_id,$package_id)
    {
         $this->db->where('client_id',$client_id);//package expiry 
         $this->db->delete('client_service_balances');//
         $this->db->where(array('client_id'=>$client_id,'package_id'=>$package_id));//package expiry 
         $this->db->delete('client_subscribers');//
    }
    
    public function cancelPendingBilling($client_id)
    {
        $this->db->where(array('client_id'=>$client_id));
        $data=array('parsed'=>1,'billing_status'=>'canceled','datetime'=>time());
        $this->db->update('billing_token',$data);
    }
 /*
     * new logic
     */
         public function partners($providerId=0){
        
        $aAr=$aEn=array();
        if($providerId)
            $this->db->where('provider_id',$providerId);
        $this->db->select(array('provider_id','provider_name_en','provider_name_ar','image','discount_share'));
        $q=$this->db->get('providers')->result();
        if($q)
        {
                return $q;
        }
        else
        return [];
    }
    /*
     * check if phone registered and activated 
     */
    public function checkPhoneAvailaibilty($phone_number)
    {
        $this->db->where(array('phone_number'=>$phone_number));
        $q=$this->db->get('clients')->result();
        if($q)
        {
            if($q[0]->phone_activated==1)
            {
                return false;
            }
            else
               if($q[0]->phone_activated==0)
               {
                   $this->db->where(array('phone_number'=>$phone_number));
                   $this->db->delete('clients');
                   return true;
               }
        }
        return true;
    }
    /*
     * addUser
     */
    public function addUser($full_name,$phone_number,$pin,$device_udid='demo',$device_token='demo')
    {
        $data=array(
                   'social_id'=>'',
                   'client_email'=>'',
                   'social_type'=>'',
                   'register_date'=>time(),
                   'device_udid'=>$device_udid,
                   'device_token'=>$device_token,
                   'user_name'=>$full_name,
                   'full_name'=>$full_name,
                   'phone_number'=>$phone_number,
                   'phone_activated'=>0,
                   'pin_sent'=>$pin
               );
        $q=$this->db->insert('clients',$data);
        $client_id=$this->db->insert_id();
        if($client_id)
            return $client_id;        
        return 0;
    }
    /*
     * get total sent
     */
    public function getTotalSentPin($client_id,$pin)
    {
        $this->db->where(array('id'=>$client_id));
        $q=$this->db->get('clients')->result();
        if($q)
        {
            if( $q[0]->total_code_sent<5)
            {
                $total_code_sent=$q[0]->total_code_sent+1;
                $this->db->where(array('id'=>$client_id));
                $this->db->update('clients',array('total_code_sent'=>$total_code_sent,'pin_sent'=>$pin));
                return true;
            }
        }
        return false;        
    }
    /*
     * resend code
     */
    public function verifyPin($client_id,$pin)
    {
        $this->db->where(array('id'=>$client_id,'pin_sent'=>$pin));
        $q=$this->db->get('clients')->result();
        if($q)
        {
            if( $q[0]->id>0)
            {
                if( $q[0]->phone_activated==1)
                    return 100;
                $this->db->where(array('id'=>$client_id));
                $this->db->update('clients',array('phone_activated'=>1));   
                return 1;
            }
        }
        return 0;
    }
    public function setClientPassword($client_id,$password)
    {
        $this->db->where(array('id'=>$client_id));
        $this->db->update('clients',array('client_password'=>$password));
        return true;
    }
    public function loginProcess($phone_number,$password)
    {
    $this->db->where(array('phone_number'=>$phone_number,'client_password'=>$password,'phone_activated'=>1));
        $q=$this->db->get('clients')->result();
        if($q)
        {
            if($q[0]->id>0)
            {
                return $q[0];
            }
           
        }
        return false;        
    }
    /*
     * return token for paasword 
     * 
     */
    public function getTokenPassword($phone_number)
    {
        $this->db->where(array('phone_number'=>$phone_number,'phone_activated'=>1));
        $q=$this->db->get('clients')->result();
        if($q)
        {
            if($q[0]->id>0)
            {
                return $q[0]->client_password;
            }
           
        }
        return false;            
    }
    
    /*
     * subscription status
     */
    public function getSubscriptionStatus($client_id)
    {
        $this->db->where(array('client_id'=>$client_id));
        $q=$this->db->get('client_subscribers')->result();
        if($q)
        {
            if($q[0]->id>0)
            {
                 if($q[0]->expiry_date>=time())
                     return 1;
                 return 2;//subscribed but expired 
                 
            }
           
        }
        return 0;//not subscribed 
    }
    /*
     * do subscription to default package
     */
    public function subscriptionTransaction($client_id,$payment_type,$subscriptionStatus=0)
    {
        $this->db->where(array('default_package'=>1));
        $q=$this->db->get('packages')->result();
        if($q)
        {
            if($q[0]->package_id>0)
            {
                $this->db->where(array('id'=>$client_id));
                $clientBalance=$this->db->get('clients')->result();
                if($clientBalance)
                {
                    if($clientBalance[0]->id>0)
                    {
                        if($clientBalance[0]->client_balance>=$q[0]->package_price)
                        {
                                $this->supscriptionTransactionProcess($client_id,$q[0]->package_price,$clientBalance[0]->client_balance,2,$q[0]->package_id,$subscriptionStatus,$q[0]->package_period_days);//2 menas deduction
                            return 1;                            
                        }
                      
                    }
                      return -1;//client hav eno engough no balance
                   
                }
            }
            return -3;//user info error 
           
        }
         return -2;//defualt package not defined 
    }
    /*
     * do deduction and logs process 
     */
    private function supscriptionTransactionProcess($client_id,$amount,$old_banalnce,$type,$package_id=0,$subscriptionStatus=0,$package_period_days=0,$discount_rate=0)
    {
        if($type==2)
            $amount=(-1)*$amount;
        
        $newBalance=$old_banalnce+$amount;
        $data=array('client_balance'=>$newBalance);
        $this->db->where(array('id'=>$client_id));
        $q=$this->db->update('clients',$data);
        
        if($q)
        {
            $renewalTime=time()+($package_period_days*24*59*59);
            if($subscriptionStatus==0)//new subscriper
            {
                
                $data=array('client_id'=>$client_id,'package_id'=>$package_id,'subscription_date'=>time(),'expiry_date'=>$renewalTime);
                $this->db->insert('client_subscribers',$data);
            }
            else
            {
                $data=array('expiry_date'=>$renewalTime);
                $this->db->where(array('client_id'=>$client_id,'package_id'=>$package_id));
                $this->db->update('client_subscribers',$data);
            }
        }
        //===
        $data=array('client_id'=>$client_id,'amount'=>$amount,'date'=>time(),'balance'=>$newBalance,'package_id'=>$package_id,'old_balance'=>$old_banalnce,'discount_rate'=>$discount_rate);
        $d=$this->db->insert('client_transaction_logs',$data);
        
        
    }
    
    /*
     * initiate first transactiuon after qr code read 
     * 
     */
        public function insertPendingTRansactionToManage($client_id,$amount,$old_banalnce,$type,$package_id=0,$subscriptionStatus=0,$package_period_days=0,$discount_rate=0,$provider,$provider_name='')
    {
        //currency jd
       $orginalAmount=$amount;
       if($discount_rate>0)
            $amount=$amount-($amount*$discount_rate);
       
        if($type==2)
            $amount=(-1)*$amount;


        //===
        $data=array('client_id'=>$client_id,'amount'=>$amount,'date'=>time(),'balance'=>0,'package_id'=>$package_id,'old_balance'=>$old_banalnce,'discount_rate'=>$discount_rate,'status'=>0,'provider_id'=>$provider,'provider_name'=>$provider_name);//pending transaction for processing 
        $d=$this->db->insert('client_transaction_logs',$data);
        
        $this->notificationSetUpClient($client_id,$provider,$amount,0);//notification for pending transaction 

    }
    /*
     * getTransactionDtails
     */
    public function getTransactionDtails($client_id,$transaction_id)
    {
            $this->db->where(array('id'=>$transaction_id,'client_id'=>$client_id));
            $clientTransaction=$this->db->get('client_transaction_logs')->result();  
            if($clientTransaction)
            {
                return $clientTransaction[0];
            }
    }
    /*
     * client change pending transaction transaction 
     */
     public function transactionProcessFinalizeWallet($client_id,$transaction_id,$status)
    {
         if($status==1)//approve only 
         {
             
                    $this->db->where(array('id'=>$transaction_id,'status'=>0));
                    $clientTransaction=$this->db->get('client_transaction_logs')->result();
                    $amount=0;
                    if($clientTransaction)
                    {
                         if($clientTransaction[0]->id>0)
                         {
                             $amount=$clientTransaction[0]->amount;
                             $discount_rate=$clientTransaction[0]->discount_rate;
                             $provider_id =$clientTransaction[0]->provider_id;
                             $provider_name =$clientTransaction[0]->provider_name;
                         
                            $orginalAmount=((-1)*$amount);//amount for provider
                            $orginalAmount+=$discount_rate*$orginalAmount;
                            $orginalAmount=ceil($orginalAmount);
                            $this->db->where(array('id'=>$client_id));
                            $clientBalance=$this->db->get('clients')->result();
                              if($clientBalance)//wallet payment
                              {
                                  if($clientBalance[0]->id>0)
                                  {
                                      if($clientBalance[0]->client_balance>=$orginalAmount)
                                      {

                                               $old_banalnce=$clientBalance[0]->client_balance;
                                               $newBalance=$old_banalnce+$amount;
                                               $data=array('client_balance'=>$newBalance);
                                               $this->db->where(array('id'=>$client_id));
                                               $q=$this->db->update('clients',$data);

                                               //=== shoul;d be update query 
                                               $this->db->where('id',$transaction_id);
                                               $array=array('status'=>$status,'change_status_time'=>time(),'balance'=>$newBalance,'old_balance'=>$old_banalnce);
                                               $this->db->update('client_transaction_logs',$array);

                                               //should log payment for providers
                                               $data=array('client_id'=>$client_id,'amount'=>$orginalAmount,'datetime'=>time(),'provider_id'=>$provider_id);
                                               $d=$this->db->insert('provider_logs',$data);
                                               return 1;
                                      }
                                      return 2;
                                  }
                                  return 2;
                              } 
                         }
                }
         }
         else
         {
             $this->db->where('id',$transaction_id);
             $array=array('status'=>$status,'change_status_time'=>time());
             $this->db->update('client_transaction_logs',$array);
             
         }
            $this->notificationSetup($client_id,$provider,$amount,$status);

    }
    
    /*
     * update transaction
     */
    public function completeTransactionProcessforNonApproved($client_id,$transaction_id,$status)
    {
             $this->db->where(array('id'=>$transaction_id,'client_id'=>$client_id));
             $array=array('status'=>$status,'change_status_time'=>time());
             $this->db->update('client_transaction_logs',$array);

             $this->db->where(array('id'=>$transaction_id,'client_id'=>$client_id));
             $provider_id=$this->db->get('client_transaction_logs')->result();
            $provider=0;
             if($provider_id)
             {
                 if($provider_id[0]->provider_id)
                     $provider=$provider_id[0]->provider_id;
             }
             $this->notificationSetup($client_id,$provider,0,$status);
    }
    /*
     * get pending transaction for clinets to take actions 
     */
    public function getClientTransactionTotal($client_id,$status,$offset=0)
    {
        $this->db->where(array('client_id'=>$client_id,'status'=>$status));
        $q=$this->db->get('client_transaction_logs')->result_array();
        return count($q);        
    }
    public function getClientTransaction($client_id,$status,$offset=0)
    {
        $totalItems=$this->getClientTransactionTotal($client_id,$status);
        $dataPerPage=10;
        $this->db->limit($dataPerPage,($offset*$dataPerPage));
        $this->db->where(array('client_id'=>$client_id,'status'=>$status));
        $this->db->select('*, FROM_UNIXTIME(`date`) as date');
        $q=$this->db->get('client_transaction_logs')->result_array();
       // $this->output->enable_profiler(TRUE);
        if($q)
        {
            $a['info']=array('total'=>$totalItems,'pages'=> ceil($totalItems/$dataPerPage));
            $a['data']=$q;
            return json_encode($a);
        }
        else
        {
            $array=array();
            return json_encode($array);
        }
    }
    
    public function time_elapsed_string($datetime, $full = false) {
    $now = new \DateTime();
    $ago = new \DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
    }    
    /*
     * generate custome message for each provider and client
     */
    public function notificationSetup($client_id,$provider,$amount,$status)
    {
         $this->notificationSetUpClient($client_id,$provider,$amount,$status);
         $this->notificationSetUpProvider($client_id,$provider,$amount,$status);
    }
    /*
     * notification area 
     */
    public function notificationSetUpClient($client_id,$provider,$amount,$status)
    {
        
    }
    public function notificationSetUpProvider($client_id,$provider,$amount,$status)
    {
        
    }
    
    /*
     * get getProviderIdFromClient
     */
    public function getProviderIdFromClient($client_id)
    {
        $this->db->where(array('id'=>$client_id));
        $q=$this->db->get('clients')->result();
        if($q)
        {
            if($q[0]->provider_id>0)
            {
                $this->db->where(array('provider_id'=>$q[0]->provider_id));
                $qProvider=$this->db->get('providers')->result();
                $provider_name='';
                if($qProvider[0]->provider_id)
                    $provider_name=$qProvider[0]->provider_name_ar;
                return [$q[0]->provider_id,$provider_name];
            }
                
        }
        else
        {
            return -1;
        }
    }
    
    /*
     * getClientDetails
     */
    public function getClientDetails($client_id)
    {
        $this->db->where(array('id'=>$client_id));
        $this->db->select(array('id','user_name','full_name','client_balance'));
        $q=$this->db->get('clients')->result();
        if($q)
        {
            if($q[0]->id>0)
            {
                $this->db->where(array('client_id'=>$q[0]->id));
                $this->db->select(array('id','subscription_date','expiry_date','status'));  
                $qSub=$this->db->get('client_subscribers')->result();
                if($qSub)
                {
                    $qSub=$qSub[0];
                    if($qSub->id>0)
                    {
                        $qSub->is_sub=true;
                        $expiryDateTime=$qSub->expiry_date;
                        $qSub->expiry_date=date('Y-m-d',$qSub->expiry_date);
                        $qSub->subscription_start=date('Y-m-d',$qSub->subscription_date);  
                        $qSub->is_expire=true;
                        if($expiryDateTime>time())
                        {
                            $qSub->is_expire=false;
                            
                        }else
                        {
                            $qSub->is_sub=false;
                        }
                             
                    }
                    else
                    {
                        $qSub->is_sub=false;
                        $qSub->expiry_date=date('Y-m-d');
                        $qSub->subscription_start=date('Y-m-d'); 
                        $qSub->is_expire=true;
                    }
                }
                else
                {

                        $qSub=array('is_sub'=>false,'expiry_date'=>date('Y-m-d'),'subscription_start'=>date('Y-m-d'),'is_expire'=>true);
                }
                return $qSub;
            }
        }
    }
    
    /*
     * unsubscribe process 
     */
    public function unSubscribeClient($clinet_id)
    {
        /*
         * need to comminucate with visa gateway incase client sub using visa
         */
        $this->db->where(array('client_id'=>$clinet_id));
        $data=array('expiry_date'=>time(),'status'=>0);
        $this->db->update('client_subscribers',$data);   
        return true;
    }
}

