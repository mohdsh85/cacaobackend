<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


if(!function_exists('logFile'))
{
    function logFile($log)
    {

//Save string to log, use FILE_APPEND to append.
        //echo __DIR__.DIRECTORY_SEPARATOR.'log_'.date("j.n.Y").'.log';
         file_put_contents(APPPATH.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'log_'.date("j.n.Y").'.log', $log, FILE_APPEND);
    }
}
if(!function_exists('lang'))
{
    function lang($key)
    {
        $ci=& get_instance();
        return $ci->lang->line($key);
    }
}

if(!function_exists('config'))
{
    function config($key)
    {
        $ci=& get_instance();
        return $ci->config->item($key);
    }
}

if(!function_exists('nav_menu'))
{
    function nav_menu()
    {
        $ci=& get_instance();
        $d= '		<ul>';
        if($ci->session->userdata('name'))
            $d.='<li>'.$ci->session->userdata('name').'</li>';
        $d.='
					<li class="fh5co-active"><a href="'.config('base_url').'">Home</a></li>
					<li><a href="'. config('base_url').('index.php/Clients/myImage').'">'.lang('photos').'</a></li>
					
                                        <li><a href="'. config('base_url').('index.php/Clients/uploadImage').'">'.lang('upload').'</a></li>
				</ul>';
        return $d;
    }
}

if(!function_exists('nav_menu_footer'))
{
    function nav_menu_footer()
    {
        return '				<div class="fh5co-footer">
				<p><small>&copy; '.date('Y'). ' '.lang('website_title').' ' . lang('all_righ_res').'</small></p>
				<ul>
					<li><a href="#"><i class="icon-facebook"></i></a></li>
					<li><a href="#"><i class="icon-twitter"></i></a></li>
					<li><a href="#"><i class="icon-instagram"></i></a></li>
					<li><a href="#"><i class="icon-linkedin"></i></a></li>
				</ul>
			</div>';
    }
}
if(!function_exists('categoryList'))
{
    function categoryList()
    {
         $ci=& get_instance();
         $data=$ci->Writers_model->getCatgeory();
         if($data)
         {
             $txt='<select name="categoryList" id="categoryList">';
             $txt.='<option value="0">'.lang('select_one').'</option>';
             foreach ($data as $loop)
             {
                 $txt.='<option value="'.$loop['id'].'">'.lang($loop['category_label']).'</option>';
             }
             $txt.='</select>';
             return $txt;
         }
    }
}

if(!function_exists('categoryListDB'))
{
    function categoryListDB()
    {
         $ci=& get_instance();
         $data=$ci->Writers_model->getCatgeory();
         if($data)
         {
             foreach($data as $loop){
                 $a[$loop['id']]=lang($loop['category_label']);
             }
         }
         return $a;
    }
}

if(!function_exists('imagePathSecure'))
{
    function imagePathSecure($id)
    {
        $securePath=md5(md5($id).config('encryption_key'));
        return config('uploaded_images').''.$securePath.'.jpg';
    }
}

if(!function_exists('imagePathSecureLoad'))
{
    function imagePathSecureLoad($id)
    {
        $securePath=md5(md5($id).config('encryption_key'));
        return config('base_url').'uploaded_images/'.$securePath.'.jpg';
    }
}

if(!function_exists('imagePathSecureLoadPath'))
{
    function imagePathSecureLoadPath($id)
    {
        $securePath=md5(md5($id).config('encryption_key'));
        return $securePath.'.jpg';
    }
}

if(!function_exists('buildImageUserList'))
{
    function buildImageUserList($data,$catView=false,$catId=0)
    {
            if($data)
            {
                $txt='';
                $cats=categoryListDB();
               // print_r($cats);
                foreach($data as $loop)
                {//onclick="renderImagePopUp(\''.imagePathSecureLoad($loop['image_id']).'\',\''.$loop['image_caption'].'\',\''.$loop['image_id'].'\',\''.secureToken($loop['image_id']).'\')" 
                  if($catView)
                    $txt.='<a href="'. config('base_url').'index.php/Gallery/showImageCat/'.secureToken($loop['image_id']).'/'.$loop['image_id'].'/'.$catId.'" ';
                  else
                      $txt.='<a href="'. config('base_url').'index.php/Gallery/showImage/'.secureToken($loop['image_id']).'/'.$loop['image_id'].'" ';
                      
                    $txt.='itemscope itemtype="http://schema.org/Person" class="gallery-item" >
					<img  data-title="Title 1" data-description="Desc 1" data-album="Album 1"   itemprop="url" src="'. imagePathSecureLoad($loop['image_id']).'" alt="'.$loop['cat_id'].'">
					<span class="overlay">
						<h2>'.($loop['image_caption']).'</h2>
						<span>'.$cats[$loop['cat_id']].' </span>
                                                <span>'.lang('comments').' ('.($loop['total_comments']).') </span>
					</span>
				</a>';
                   
                }
                return $txt;
            }
    }
}
if(!function_exists('secureToken'))
{
    function secureToken($id)
    {
        return md5(md5($id).config('encryption_key').'shareif');
    }
}
if(!function_exists('change_counter_db_value_after_user_upload'))
{
    function change_counter_db_value_after_user_upload($category_id)
    {
                 $ci=& get_instance();
                $data=$ci->Writers_model->updateCountersUserUpload($category_id);
    }
}

if(!function_exists('buildComments'))
{
    function buildComments($commnets,$image_id)
    {
        $txt='';
        if($commnets)
        {
            
            foreach($commnets as $loop)
            {
                $txt.=' <li class="list-group-item">
                        <div class="row">
                            <div class="col-xs-2 col-md-1">
                                <img src="http://placehold.it/80" class="img-circle img-responsive" alt="" /></div>
                            <div class="col-xs-10 col-md-11">

                                <div class="comment-text">
                                   '.$loop['comment'].'
                                </div>
                                <div>
                                    
                                    <div class="mic-info">
                                        '.lang('by').' '.$loop['user_name'].'  '.print_date($loop['posted_time']).'
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>';
            }
        }
        return $txt;
    }
}

if(!function_exists('print_date'))
{
    function print_date($time)
    {
        return date('Y/M/d',$time);
    }
}

if(!function_exists('commentBox'))
{
    function commentBox($imageId,$total_comments=0)
    {
        return '<li class="list-group-item">
                        <div class="row">
                            <div class="col-xs-2 col-md-1">
                                <img src="http://placehold.it/80" class="img-circle img-responsive" alt="" /></div>
                            <div class="col-xs-10 col-md-11">

                                <div class="comment-text">
                                   <textarea id="commentTxt" style="width:100%;height:50px" placeholder="'. lang('write_comment').'"></textarea>
                                </div>
                                <div>
                                     <div class="mic-info">
                                       <input type="button" name="send" id="send" onclick="sendComment(\''.$imageId.'\',\''. secureToken($imageId).'\',\''.lang('need_to_login').'\',\''.lang('fill_comment_txt').'\',\''.lang('write_comment').'\')" value="'.lang('send_comment').'" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>';
    }
}
if(!function_exists('check_login'))
{
    function check_login()
    {
        $ci=& get_instance();
        if($ci->session->userdata(config('session_user_id')))
        {
            return true;
        }
        return false;
    }
}