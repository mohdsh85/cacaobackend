<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Clients
 *
 * @author shareifhome
 */
class Clients extends CI_Controller {
    //put your code here
    public function __construct() {
        parent::__construct();
        // $this->load->library('session');
      //   print_r($this->session);
    }
    
    public function _remap($method, $params = array())
    {
      //  echo 'ss'.$this->session->userdata[config('session_user_id')];
       // exit();
        if(!$this->session->userdata(config('session_user_id')))
        {
            $data['info']=array();
            $this->load->view('header');
            $this->load->view('login',$data);
            $this->load->view('footer');
            return;
        }
        else
        if (method_exists($this, $method))
        {
                return call_user_func_array(array($this, $method), $params);
        }
        show_404();
    }    

    public function uploadImage()
    {
            $data['info']=array('intro_message'=> sprintf(lang('intro_upload'),$this->session->userdata('name')));
            $this->load->view('header');
            $this->load->view('uploadImage',$data);
            $this->load->view('footer');
    }
    
    public function myImage()
    {
            $data['client_images']= buildImageUserList($this->Writers_model->getUplopadedImages($this->session->userdata(config('session_user_id'))));
            $this->load->view('header');
            $this->load->view('client_images',$data);
            $this->load->view('footer');
    }

    public function imagesCategory($catId)
    {
            $data['client_images']= buildImageUserList($this->Writers_model->getUplopadedImagesByCategory($catId),true,$catId);
            $this->load->view('header');
            $this->load->view('client_images',$data);
            $this->load->view('footer');
    }
    public function saveImage()
    {
        $cat=$this->input->post('cat');
        $image=$this->input->post('imageSrc');
        $image_caption=$this->input->post('image_caption');
        if($cat>0)
        {
            $data=array('image_caption'=>$image_caption,'user_id'=>$this->session->userdata(config('session_user_id')),'cat_id'=>$cat,'uploaded_time'=>time());
            $id=$this->Writers_model->saveImageDb($data);
            $output_file= imagePathSecure($id);
            $imageFile=$this->base64_to_jpeg($image, $output_file);
            echo lang('image_uploaded_success');
            change_counter_db_value_after_user_upload($cat);
            return;
        }
        echo  lang('failed_to_upload_image');;
        
        
        
    }
    
        function base64_to_jpeg($base64_string, $output_file) {
        // open the output file for writing
        $ifp = fopen( $output_file, 'wb' ); 

        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );

        // we could add validation here with ensuring count( $data ) > 1
        fwrite( $ifp, base64_decode( $data[ 1 ] ) );

        // clean up the file resource
        fclose( $ifp ); 

        return $output_file; 
        }
        
        public function showImagePopup()
        {
            $secureImage=$this->input->post('secureImage');
            $caption=$this->input->post('caption');
            $imageId=$this->input->post('imageId');
            $token=$this->input->post('token');
            if(secureToken($imageId)==$token)
            {
                $data['image']=$secureImage;
                $getComments=$this->Writers_model->getComment($imageId);;
                 $getTotalComments=$this->Writers_model->getTotalComments($imageId);;
                $data['total_comments']= $getTotalComments;
                $data['comments']=$getComments;
                $data['imageId']=$imageId;
                $string=$this->load->view('popUpImageGallery',$data,true);
                echo $string;
                return;
            }
            else
            {
                $data['image']=$secureImage;
                $getComments='';
                $data['total_comments']='0';
                $string=$this->load->view('popUpImageGallery',$data,true);
                echo $string;
                return;
            }

        }
}
