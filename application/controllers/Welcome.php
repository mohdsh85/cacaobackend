<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Writers_model');
          //   $this->load->library('session');
            
        }
        
        
        
        public function index()
	{

            echo 'hi';
            return;
            $newdata = array(
            config('session_user_id')  => '1',
            'email'     => 'johndoe@some-site.com',
            'logged_in' => TRUE,
                'name'=>'mohammed al shareif'
            );

            $this->session->set_userdata($newdata);
          // print_r($_SESSION);
            $data['category']=$this->loadCategory();
            $this->load->view('header');
            $this->load->view('welcome_message',$data);
            $this->load->view('footer');
	}
        
        public function logout()
        {
            session_destroy();
            $data['category']=$this->loadCategory();
            $this->load->view('header');
            $this->load->view('welcome_message',$data);
            $this->load->view('footer');
        }

        
        public function loadCategory()
        {
            $data=$this->Writers_model->getCatgeory();
            if($data)
            {
                $txt='';
                foreach($data as $loop)
                {
                    $txt.='<a itemscope itemtype="http://schema.org/Person" class="gallery-item" href="'.config('base_url').'index.php/Clients/imagesCategory/'.$loop['id'].'">
					<img itemprop="url" src="'. config('assets_path').'cat_images/'.$loop['id'].'.jpg" alt="'.$loop['category_label'].'">
					<span class="overlay">
						<h2>'.lang($loop['category_label']).'</h2>
						<span>'.$loop['total_posts'].' '.lang('photos').'</span>
					</span>
				</a>';
                   
                }
            }
          //  $data[0]['image_path']= $this->config->item('base_url').($data[0]['image_path']);
            $this->addToLog(__CLASS__,__FUNCTION__);
            return $txt;//json_encode($data);
            
        }
        public function getWriters()
        {
            $array=array(array('id'=>1,'name'=>'Mohammed'),array('id'=>2,'name'=>'hareif'));
            echo json_encode($array);
        }
        public function getWriterDetails($id)
        {
            $data=$this->Writers_model->getWriterInfo($id);
            $data[0]['image_path']= $this->config->item('base_url').($data[0]['image_path']);
            echo json_encode($data);
//            $array=array(array('id'=>2,'name'=>'x mohammed محمد'));
//            echo json_encode($array);
            $this->addToLog(__CLASS__,__FUNCTION__,$id);
        }
        
        public function loadWriters()
        {
            $data=$this->Writers_model->getWriters();
            if($data)
            {
                $txt='';
                foreach($data as $loop)
                {
                   $txt.='<li><a href="javascript:void(0)" onclick="getWriterProfile('.$loop['id'].')">'.$loop['writer_name'].' </a></li>'; ;
                }
            }
          //  $data[0]['image_path']= $this->config->item('base_url').($data[0]['image_path']);
            echo $txt;//json_encode($data);
            $this->addToLog(__CLASS__,__FUNCTION__);
        }
        
        public function loadBooks()
        {
            $data=$this->Writers_model->getBooks();
            if($data)
            {
                $txt='';
                foreach($data as $loop)
                {
                   $txt.='<li><a href="javascript:void(0)" onclick="bookLandingPage('.$loop['book_id'].');">'.$loop['book_name'].' </a></li>'; ;
                }
            }
          //  $data[0]['image_path']= $this->config->item('base_url').($data[0]['image_path']);
            echo $txt;//json_encode($data);     
            $this->addToLog(__CLASS__,__FUNCTION__);
        }
        
        public function loadBooksCover()
        {

                                
             $data=$this->Writers_model->getBooks();
            if($data)
            {
                $txt='<div class="owl-stage-outer">';
                foreach($data as $loop)
                {
                    $loop['image_path']= $this->config->item('base_url').($loop['image_path']);
                    $txt.='<div class="single-hero-post" onclick="bookLandingPage('.$loop['book_id'].')" id="bookCover">
                    <div class="slide-img bg-img" style="background-image: url('.$loop['image_path'].');"></div>
                    <div class="hero-slides-content">
                         <p></p>
                             <a href="javascript:void(0)" class="post-title">
                              <h4>'.$loop['book_id'].' ' .$loop['book_cover_desc'].'</h4>
                         </a>
                    </div></div>';
                   
                }
                $txt.='</div>';
            }
          //  $data[0]['image_path']= $this->config->item('base_url').($data[0]['image_path']);
            echo $txt;//json_encode($data);  
            $this->addToLog(__CLASS__,__FUNCTION__);
        }
        
        public function loadBookChapters($book_id)
        {
            $data=$this->Writers_model->getChaptersBook($book_id);
            if(sizeof($data))
            {
                $txt='';
                foreach($data as $loop)
                {
                    
                    $txt.='<li ><a  href="javascript:void(0)" onclick="getChaptersData('.$loop['chapter_id'].','.$book_id.')">'.$loop['chapter_title'].'</a></li>';
                   
                }
            }
            else
            {
            $txt='
                    <li><a href="#">لا يوجد أقسام</a></li>
                 ';                
            }

            echo $txt;
            $this->addToLog(__CLASS__,__FUNCTION__,$book_id);
        }
        
        public function getChaptersTxt($chapter_id,$book_id)
        {
            $data=$this->Writers_model->getChaptersTxt($chapter_id);
            if(sizeof($data))
            {
                $txt='';

                echo json_encode($data);  

            }
            else
            {
            $txt='
                    <li><a href="#">لا يوجد أقسام</a></li>
                 ';                
            }
        $this->addToLog(__CLASS__,__FUNCTION__,$book_id,$chapter_id);
                      
        }
        
        public function getFirstChapter($book_id)
        {
             $data=$this->Writers_model->getFirstChapter($book_id);
             echo $data[0]['chapter_id'];
             $this->addToLog(__CLASS__,__FUNCTION__,$book_id,$data[0]['chapter_id']);
        }
        
        public function bookLandingPage($book_id)
        {
            $data=$this->Writers_model->getBookData($book_id);
            $data[0]['image_path']= $this->config->item('base_url').($data[0]['image_path']);
            echo json_encode($data);
            $this->addToLog(__CLASS__,__FUNCTION__,$book_id);
        }
        
        public function addToLog($controller,$method,$book_id=0,$chapter_id=0)
        {
            
                $detect = new Mobile_Detect();
                $isMobile=$detect->isMobile();
                if($isMobile)
                {
                    $isAndroid=$detect->is('AndroidOS');
                    if($isAndroid)
                        $type='android';
                    else
                        $type= 'ios';
                }
                else
                {
                    $type= 'browser';
                }
            $ip_address=$this->get_client_ip();
            $this->Writers_model->insertToLog($controller,$method,$book_id,$chapter_id,$ip_address,$type);
        }
        
        public function get_client_ip() {
                $ipaddress = '';
                if (getenv('HTTP_CLIENT_IP'))
                    $ipaddress = getenv('HTTP_CLIENT_IP');
                else if(getenv('HTTP_X_FORWARDED_FOR'))
                    $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
                else if(getenv('HTTP_X_FORWARDED'))
                    $ipaddress = getenv('HTTP_X_FORWARDED');
                else if(getenv('HTTP_FORWARDED_FOR'))
                    $ipaddress = getenv('HTTP_FORWARDED_FOR');
                else if(getenv('HTTP_FORWARDED'))
                   $ipaddress = getenv('HTTP_FORWARDED');
                else if(getenv('REMOTE_ADDR'))
                    $ipaddress = getenv('REMOTE_ADDR');
                else
                    $ipaddress = 'UNKNOWN';
                return $ipaddress;
            }
            public function test()
            {
                            $this->load->view('header');
            $this->load->view('test',$data);
          //  $this->load->view('footer');
            }
            
            
        public function privacy()
	{

          
            $this->load->view('header');
           // $this->load->view('welcome_message',$data);
            $this->load->view('footer');
	}
                    
            
        public function terms()
	{

          echo 'ss';
            $this->load->view('header');
           // $this->load->view('welcome_message',$data);
            $this->load->view('footer');
	}
}
