<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Gallery
 *
 * @author shareifhome
 */
class Gallery extends CI_Controller {
    //put your code here
         public function __construct() {
        parent::__construct();
        // $this->load->library('session');
      //   print_r($this->session);
    }
    
    public function _remap($method, $params = array())
    {
      //  echo 'ss'.$this->session->userdata[config('session_user_id')];
       // exit();

        if (method_exists($this, $method))
        {
                return call_user_func_array(array($this, $method), $params);
        }
        show_404();
    }  
    public function showImageCat($secureImage,$imageId,$cat)
    {
        if(secureToken($imageId)!=$secureImage)
            {
                $data['message']= lang('error_found_desc');
                $data['heading']= lang('error_found');
                $this->load->view('header');
                $this->load->view('errors/html/error_general',$data);
                $this->load->view('footer');
                return;
            }
            $data['description']=$this->Writers_model->getDescriptionImage($imageId);
            $data['image']=imagePathSecureLoad($imageId);
            $data['url']= config('base_url').'index.php/'.__CLASS__.'/'.__FUNCTION__.'/'.$secureImage.'/'.$imageId.'/'.$cat;
            $getComments=$this->Writers_model->getComment($imageId);;
            $getTotalComments=$this->Writers_model->getTotalComments($imageId);;
            $data['total_comments']= $getTotalComments;
            $data['comments']=$getComments;
            $data['imageId']=$imageId;
            $data['client_images']= buildImageUserList($this->Writers_model->getUplopadedImagesByCategory($cat),true,$cat);
            $this->load->view('header');
            $this->load->view('popUpImageGallery',$data);
            $this->load->view('footer');
    }

        public function showImage($secureImage,$imageId)
    {
         if(secureToken($imageId)!=$secureImage)
            {
                $data['message']= lang('error_found_desc');
                $data['heading']= lang('error_found');
                $this->load->view('header');
                $this->load->view('errors/html/error_general',$data);
                $this->load->view('footer');
                return;
            }
            $data['description']=$this->Writers_model->getDescriptionImage($imageId);
            $data['image']=imagePathSecureLoad($imageId);
            $data['url']= config('base_url').'index.php/'.__CLASS__.'/'.__FUNCTION__.'/'.$secureImage.'/'.$imageId;
            $getComments=$this->Writers_model->getComment($imageId);;
            $getTotalComments=$this->Writers_model->getTotalComments($imageId);;
            $data['total_comments']= $getTotalComments;
            $data['comments']=$getComments;
            $data['imageId']=$imageId;
            $data['client_images']= buildImageUserList($this->Writers_model->getUplopadedImages($this->session->userdata(config('session_user_id'))));
            $this->load->view('header');
            $this->load->view('popUpImageGallery',$data);
            $this->load->view('footer');
    }
            
}
