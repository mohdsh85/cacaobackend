<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApiController
 *
 * @author shareifhome
 */
class ApiController extends CI_Controller {
    //put your code here
    
    public function __construct() {
        parent::__construct();
        
        $this->load->database();
        
    }
    public function _remap($method, $params = array())
    {

        
        if (method_exists($this, $method))
        {
                return call_user_func_array(array($this, $method), $params);
        }
        show_404();
    } 
    
    /*
     * login register with same functiuon 
     */
    public function loginResgister()
    {
        $user_name=$this->input->get('user_name');
        $user_email=$this->input->get('user_email');
        $user_socail_id=$this->input->get('user_social_id');
        $type=$this->input->get('user_type');//facebook or gmail 1,2
        $device_udid=$this->input->get('device_udid');
        $device_token=$this->input->get('device_token');
        if($type==0)
            $type=1;
        $whereRegisterArray=array('social_id'=>$user_socail_id,'social_type'=>$type);
        $existedUser=$this->ApiModel->checkRegisterUser($whereRegisterArray);
        $mobileType=new Mobile_Detect();
        $deviceType='Android';
        if($mobileType->is('AndroidOS'))
            $deviceType='Android';
        if($mobileType->is('iOS'))
            $deviceType='Ios';
       // echo $deviceType;
        if($existedUser>0)//gather user info
        {
            //echo $existedUser;
           $info=$this->gatherInfoForClient($existedUser);
           //==========log
           $log  = "already registerd User: ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                   "client_id=".$info['client_id'].PHP_EOL.
                    "client_info=".json_encode($info).PHP_EOL.
            "-------------------------".PHP_EOL;
                //Save string to log, use FILE_APPEND to append.
               logFile($log);
           //==========
        }else//new user and return info
        {
            $insertUser=$this->ApiModel->createUser($user_name,$user_email,$user_socail_id,$type,$device_token,$device_udid);
            $info=$this->gatherInfoForClient($insertUser);
            
           $log  = "new User: ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                   "client_id=".$info['client_id'].PHP_EOL.
                   "client_info=".json_encode($info).PHP_EOL.
            "-------------------------".PHP_EOL;
                //Save string to log, use FILE_APPEND to append.
               logFile($log);            
        }
        
        $info['status']=true;
        $info['need_login']=false;
        echo json_encode($info);
    }
    /*
     * returnb required info for users 
     */
    private function gatherInfoForClient($client_id)
    {
        $data['client_id']=$client_id;
        $data['balances_ar']=$data['balances_en']='';
        $data['secure_token']=$this->secureToken($client_id);//pass secure token to be used in APIs
        //return subscription statu
        $getSubscriptionData=$this->ApiModel->getSubscriptionData($data['client_id']);
        $data['no_subscription_ar']='';
        $data['no_subscription_en']='';        
        if($getSubscriptionData)
        {
            
            $data['package_id']=$getSubscriptionData['package_id'];
            $packageDetails=$this->ApiModel->getPackages($data['package_id'])[0];
            $data['subscription_date']=date('Y-m-d',$getSubscriptionData['subscription_date']);
            $data['expiry_date']=date('Y-m-d',$getSubscriptionData['expiry_date']);
            $data['is_active_subscriber']=( ($getSubscriptionData['expiry_date']>time())?true:FALSE);
            $data['is_subscribe']=true;
            if($packageDetails)
            {
                foreach($packageDetails as $key=>$value)
                {
                    $data[$key]=$value;
                }
            }
            //=========get client balances 
            
            if($data['service_list'])
            {
                $balances=$this->ApiModel->buildBalances($data['client_id'],$data['service_list']);
                $data['balances_ar']=$balances[0];
                $data['balances_en']=$balances[1];
            }
        }
        else
        {
            $data['package_id']=0;
            $data['package_ar']='';
            $data['package_en']='';
            $data['subscription_date']=0;
            $data['expiry_date']=0;
            $data['is_subscribe']=false;
            $data['is_active_subscriber']=false;
            $data['desc_en']='';
            $data['desc_ar']=$data['service_list']=$data['package_price']=$data['is_offer']=$data['package_period_days']=$data['package_period_label_en']=$data['package_period_label_ar']=$data['servicesArLabels']=$data['servicesEnLabels']='';
            $data['no_subscription_ar']='no subscription package';
            $data['no_subscription_en']='no subscription package';
        }
        return $data;
    }
    
    /*
     * public function send qr
     */
    public function sendQr()
    {
        $client_id= $this->input->get('client_id');
        $secure_token=$this->input->get('secure_token');
        $qr_value=$this->input->get('qr_value');//service_provider value
        if($this->isAuthinticated($client_id,$secure_token) && $qr_value)
        {
            $qrValues=explode('_',$qr_value);
            $service_id=$qrValues[0];
            $provider_id=$qrValues[1];
            //echo $client_id.'==>'.$service_id.'==>'.$provider_id;
            $gatherInfo=$this->gatherInfoForClient($client_id);
            
            if($gatherInfo['is_subscribe']==FALSE)// client not subscribed 
            {
                echo json_encode(array('message_ar'=>'Please Subscribe First',
                                            'message_en'=>'Please Subscribe First','status'=>false,'need_login'=>false));
                $log  = "not subscribed client: ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                                   "client_id=".$gatherInfo['client_id'].PHP_EOL.
                                    "service_id=".$service_id.PHP_EOL.
                                   "client_info=".json_encode($gatherInfo).PHP_EOL.
                            "-------------------------".PHP_EOL;
                                //Save string to log, use FILE_APPEND to append.
                               logFile($log,'_'.__FUNCTION__);                            
                return;
            }
            if($gatherInfo['is_active_subscriber']==FALSE)// client not subscribed 
            {
                echo json_encode(array('message_ar'=>'Please Renew Subscription First',
                                            'message_en'=>'Please Renew Subscription First','status'=>false,'need_login'=>false));
            $log  = "expired subscription client: ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                                              "client_id=".$gatherInfo['client_id'].PHP_EOL.
                                               "service_id=".$service_id.PHP_EOL.
                                              "client_info=".json_encode($gatherInfo).PHP_EOL.
                                       "-------------------------".PHP_EOL;
                                           //Save string to log, use FILE_APPEND to append.
                                           logFile($log,'_'.__FUNCTION__);                          
                return;
            }
            if(!isset($gatherInfo['service_'.$service_id]))
            {
                echo json_encode(array('message_ar'=>'you are not in this service',
                                            'message_en'=>'you are not in this service','status'=>false,'need_login'=>false));
            $log  = "client not in service: ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                                              "client_id=".$gatherInfo['client_id'].PHP_EOL.
                                               "service_id=".$service_id.PHP_EOL.
                                              "client_info=".json_encode($gatherInfo).PHP_EOL.
                                       "-------------------------".PHP_EOL;
                                           //Save string to log, use FILE_APPEND to append.
                                           logFile($log,'_'.__FUNCTION__);                   
                return;                
            }
            $getCurrentBalance=$this->getCurrentBalance($client_id,$service_id);
            if($getCurrentBalance<=0)
            {
                echo json_encode(array('message_ar'=>'All Balance Consumed',
                                            'message_en'=>'Balance Consumed','status'=>false,'need_login'=>false));
            $log  = "client consumed balance for service ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                                              "client_id=".$gatherInfo['client_id'].PHP_EOL.
                                               "service_id=".$service_id.PHP_EOL.
                                              "client_info=".json_encode($gatherInfo).PHP_EOL.
                                       "-------------------------".PHP_EOL;
                                           //Save string to log, use FILE_APPEND to append.
                                           logFile($log,'_'.__FUNCTION__);                  
                return;                
            }
            
            //------ deduction process
          // print_r($gatherInfo);
            $this->updateClientBalanceTable($client_id,$service_id);
            $this->logProviderClientData($provider_id,$client_id,$service_id);
            $remainingBalance=$getCurrentBalance-1;//remainig balance
            $serviceLabels=$this->ApiModel->services($service_id);
           // print_r($serviceLabels);
            $msgAr=sprintf(lang('ticket_used_ar'), $serviceLabels[0][$service_id]);
            //echo $msgAr;
            //return;
            $msgEn=sprintf(lang('ticket_used'), $serviceLabels[1][$service_id]);
            $balanceAr=sprintf(lang('your_ticket_balance_ar'),$remainingBalance);
            $balanceEn=sprintf(lang('your_ticket_balance'),$remainingBalance);
            $validAr=sprintf(lang('valid_till_ar'),$gatherInfo['expiry_date']);
            $validEn=sprintf(lang('valid_till'),$gatherInfo['expiry_date']);
             echo json_encode(array('message_ar'=> $msgAr,'message_en'=>$msgEn,'status'=>true,'need_login'=>false,
                 'ticket_balance_ar'=> $balanceAr,
                 'ticket_balance'=> $balanceEn,
                 'valid_till_ar'=> $validAr,
                 'valid_till'=> $validEn
                 ));
             //push msg
                         $log  = "client got the service ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                                              "client_id=".$gatherInfo['client_id'].PHP_EOL.
                                               "service_id=".$service_id.PHP_EOL.
                                              "client_info=".json_encode($gatherInfo).PHP_EOL.
                                       "-------------------------".PHP_EOL;
                                           //Save string to log, use FILE_APPEND to append.
                                           logFile($log,'_'.__FUNCTION__);   
             return;
        }
        else
        {
            echo json_encode(array('message_ar'=>'Try to Logout and login Again Again, Authintication Error.'
                ,'message_en'=>'Try to Logout and login Again Again, Authintication Error.','status'=>false,'need_login'=>true));
        }
        
    }
    /*
     * getCurrentBalance
     */
    private function getCurrentBalance($client_id,$service_id)
    {
        $this->db->where(array('client_id'=>$client_id,'service_id'=>$service_id));
        $q=$this->db->get('client_service_balances')->row_array();
        return (int)$q['balance'];
    }
    /*
     * updateClientBalanceTable
     */
    private function updateClientBalanceTable($client_id,$service_id)
    {
        $this->db->where(array('client_id'=>$client_id,'service_id'=>$service_id));
        $this->db->set('balance','balance-1',false);
        $this->db->update('client_service_balances');
    }

    
    private function logProviderClientData($provider_id,$client_id,$service_id)
    {
        $data=array('provider_id'=>$provider_id,'client_id'=>$client_id,'service_id'=>$service_id,'paid'=>0,'datetime'=>time());
        $this->db->insert('provider_logs',$data);
        $data=array('provider_id'=>$provider_id,'client_id'=>$client_id,'service_id'=>$service_id,'datetime'=>time());
        $this->db->insert('client_logs',$data);
    }
    /*
     * return available packages on the system 
     */
    public function packageList()
    {
        echo json_encode($this->ApiModel->getPackages('',1));
    }

    /*
     * billing token generator
     */
    public function generateToken()
    {
       
        $client_id= $this->input->get('client_id');
        $package_id= $this->input->get('package_id');
        $secure_token=$this->input->get('secure_token');
        if($this->isAuthinticated($client_id,$secure_token) && (($package_id))>0)
        {
            $gatherInfo=$this->gatherInfoForClient($client_id);
            if($gatherInfo['is_active_subscriber']==TRUE || $gatherInfo['is_subscribe']==TRUE)// client subscribed 
            {
                echo json_encode(array('message_ar'=>'Please unsub from your package before',
                                            'message_en'=>'Please unsub from your package before','status'=>false,'need_login'=>false));
                $log  = "client already sub ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                            "client_id=".$gatherInfo['client_id'].PHP_EOL.
                             "package_id=".$package_id.PHP_EOL.
                            "client_info=".json_encode($gatherInfo).PHP_EOL.
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);
                
                return;
            }
            //---check if user have pending process 
            $getPendingBilling=$this->ApiModel->getPendingBillings($client_id);
            if($getPendingBilling>0)//if the client have pending billings
            {
                echo json_encode(array('message_ar'=>'you have pending process please wait',
                                            'message_en'=>'you have pending process please wait','status'=>false,'need_login'=>false));
                        $log  = "client pending sub ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                            "client_id=".$gatherInfo['client_id'].PHP_EOL.
                             "package_id=".$package_id.PHP_EOL.
                            "client_info=".json_encode($gatherInfo).PHP_EOL.
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);
                return;
            }
            $token= md5(uniqid(rand().$client_id.$package_id, true));
            $data=array('client_id'=>$client_id,'package_id'=>$package_id,'token'=>$token,'parsed'=>0,'billing_status'=>0,'datetime'=>time());
            $this->db->insert('billing_token',$data);
            echo json_encode(array('message_ar'=>'token generated'
                ,'message_en'=>'token generated','status'=>true,'token'=>$token));
                    $log  = "client generet token sub ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                            "client_id=".$gatherInfo['client_id'].PHP_EOL.
                             "package_id=".$package_id.PHP_EOL.
                            "client_info=".json_encode($gatherInfo).PHP_EOL.
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);
        }
        else
        {
            echo json_encode(array('message_ar'=>'Try to Logout and login Again Again, Authintication Error.'
                ,'message_en'=>'Try to Logout and login Again Again, Authintication Error.','status'=>false,'need_login'=>true));
        }
        
    }
    

    /*
     * get billing confirmation
     */
    public function confirmBilling()
    {
       // print_r($_POST);
        $token=$this->input->get('token');       
        $status=$this->input->get('status');       
        $payment_reference=$this->input->post('payment_reference');
        $checkValidity=$this->ApiModel->checkTokenValidity($token);
        if($checkValidity==0)        
        {
             echo json_encode(array('message_ar'=>'invalid token.'
                ,'message_en'=>'invalid token.','status'=>false,'need_login'=>false));
                     $log  = "invalid token ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                            "token=".$token.PHP_EOL.
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);
             return;
        }
        $this->ApiModel->processBilling($token,$status,$payment_reference);
                echo json_encode(array('message_ar'=>'billing done'
                ,'message_en'=>'billing done','status'=>true,'need_login'=>false));
                $log  = "suvccess token ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                            "token=".$token.PHP_EOL.
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);
             return;
    }
    /*
     * unsub from package
     */
    public function cancelSubscription()
    {
        $client_id= $this->input->get('client_id');
        $package_id= $this->input->get('package_id');
        $secure_token=$this->input->get('secure_token');
        if($this->isAuthinticated($client_id,$secure_token) && (($package_id))>0)
        {
            $this->ApiModel->cancelSubscription($client_id,$package_id);
             echo json_encode(array('message_ar'=>'successfuly unsub.'
                ,'message_en'=>'successfuly unsub.','status'=>true,'need_login'=>false));
             $log  = "cancel sub  ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                            "client_id=".$client_id.PHP_EOL.
                       "package_id=".$package_id.PHP_EOL.
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);
             //push
        }
        else
        {
             echo json_encode(array('message_ar'=>'Try to Logout and login Again Again, Authintication Error.'
                ,'message_en'=>'Try to Logout and login Again Again, Authintication Error.','status'=>false,'need_login'=>true));
        }
    }
    /*
     * public function cancel pending billing tokens
     */
    public function cancelPendingBillings()
    {
        $client_id= $this->input->get('client_id');
        $secure_token=$this->input->get('secure_token');
        if($this->isAuthinticated($client_id,$secure_token))
        {
            $this->ApiModel->cancelPendingBilling($client_id);
             echo json_encode(array('message_ar'=>'successfuly canceled.'
                ,'message_en'=>'successfuly canceled.','status'=>true,'need_login'=>false));
             $log  = "cancel sub  ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                            "client_id=".$client_id.PHP_EOL.
                       
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);
             //push
        }
        else
        {
             echo json_encode(array('message_ar'=>'Try to Logout and login Again Again, Authintication Error.'
                ,'message_en'=>'Try to Logout and login Again Again, Authintication Error.','status'=>false,'need_login'=>true));
        }
    }
    /*
     * public function used tickect 
     */
    public function usedTicketHistory()
    {
        $client_id= $this->input->get('client_id');
        $secure_token=$this->input->get('secure_token');
        if($this->isAuthinticated($client_id,$secure_token))
        {
            $history=$this->ApiModel->getClientLogs($client_id);
            if(sizeof($history))
            {
                $history['page_label_ar']=array('page_label_ar'=>'used ticket');
                $history['page_label']=array('page_label'=>'used ticket');
                $history['status']=true;
                $history['need_login']=false;
                echo json_encode(array_values($history));
            }
            else
            {
                echo json_encode(array('message_ar'=>'no records found'
                ,'message_en'=>'sno records found','status'=>true,'need_login'=>false));
            }
             
             $log  = "showing ticket history  ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                            "client_id=".$client_id.PHP_EOL.
                       
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);
             //push
        }
        else
        {
             echo json_encode(array('message_ar'=>'Try to Logout and login Again Again, Authintication Error.'
                ,'message_en'=>'Try to Logout and login Again Again, Authintication Error.','status'=>false,'need_login'=>true));
        }
    }
    /*
     * 
     */
    public function privacyPolicyAgrement()
    {
        echo json_encode($this->ApiModel->privacyPolicyAgrement());
    }
    /*
     * 
     * encrypt and decrpt values
     */
    public function encryptVisaValue()
    {
        $this->encryption->initialize(array('driver' => 'mcrypt'));
        $plain_text = 'mohammed al shareif';
        $ciphertext = $this->encryption->encrypt($plain_text);
        ECHO $ciphertext;
        // Outputs: This is a plain-text message!
       // echo $this->encryption->decrypt($ciphertext);
    }

    /*
     * get updated user data 
     */
    public function updateUserData()
    {
         $client_id= $this->input->get('client_id');
        $secure_token=$this->input->get('secure_token');
        if($this->isAuthinticated($client_id,$secure_token))
        {
            $info=$this->gatherInfoForClient($client_id);
            
            echo json_encode($info);
        }
        else
        {
             echo json_encode(array('message_ar'=>'Try to Logout and login Again Again, Authintication Error.'
                ,'message_en'=>'Try to Logout and login Again Again, Authintication Error.','status'=>false,'need_login'=>true));
        }
        
    }
    
    public function getClientBalances()
    {
        $client_id= $this->input->get('client_id');
        $secure_token=$this->input->get('secure_token');
        if($this->isAuthinticated($client_id,$secure_token))
        {
            $data['client_id']=$client_id;
            $data['balances']=array();
            $data['secure_token']=$this->secureToken($client_id);//pass secure token to be used in APIs
            //return subscription statu
            $getSubscriptionData=$this->ApiModel->getSubscriptionData($data['client_id']);
            if($getSubscriptionData)
            {

                $data['package_id']=$getSubscriptionData['package_id'];
                $packageDetails=$this->ApiModel->getPackages($data['package_id'])[0];
                $data['subscription_date']=date('Y-m-d',$getSubscriptionData['subscription_date']);
                $data['expiry_date']=date('Y-m-d',$getSubscriptionData['expiry_date']);
                $data['is_active_subscriber']=( ($getSubscriptionData['expiry_date']>time())?true:FALSE);
                $data['is_subscribe']=true;
                if($packageDetails)
                {
                    foreach($packageDetails as $key=>$value)
                    {
                        $data[$key]=$value;
                    }
                }
                //=========get client balances 

                if($data['service_list'] && $data['is_active_subscriber'])
                {
                    $data['balances']=$this->ApiModel->buildBalances($data['client_id'],$data['service_list']);
                }
                
            }
            else
            {
                

            }
        }
        else
        {
             echo json_encode(array('message_ar'=>'Try to Logout and login Again Again, Authintication Error.'
                ,'message_en'=>'Try to Logout and login Again Again, Authintication Error.','status'=>false,'need_login'=>true));
             return;
        }
            echo json_encode($data['balances']);
    }
    /*
     * return required info for mechinate 
     */
    
    public function getMerchintInfio() {
        $client_id= $this->input->get('client_id');
        $secure_token=$this->input->get('secure_token');
        if($this->isAuthinticated($client_id,$secure_token))
        {
            $transaction_title='Subscription Process';
            $curencyCode='USD';
            $customer_email=$client_id.'@scan_parks.info';
            $dbEmail=$this->getClientEmail($client_id);
            if($dbEmail)
               $customer_email=$dbEmail;
            $customer_phone_number='9627'.$client_id;
             $fields=array('transaction_title'=>$transaction_title,'currency_code'=>$curencyCode,'customer_email'=>$customer_email,
                 'customer_phone_number'=>$customer_phone_number,'merchant_email'=>'mohdsh85@gmail.com','secret_key'=>'HQLxsmlEsCk2lXGsAskdVrxBbAqxcUITLurdRNSjqclLa2dKuxYpYw1a6sHjmqd7UFlLsd4TqqEAsRlXpkmnQoyWRffeFtmA8IlB');
             echo json_encode($fields);
        }
           else
        {
             echo json_encode(array('message_ar'=>'Try to Logout and login Again Again, Authintication Error.'
                ,'message_en'=>'Try to Logout and login Again Again, Authintication Error.','status'=>false,'need_login'=>true));
        }
    }
    
    /*
     * get client email
     */
    private function getClientEmail($client_id)
    {
        $this->db->where('id',$client_id);
        $q=$this->db->get('clients')->result_array()[0];
       // print_r($q);
        if($q['client_email']!='')
            return $q['client_email'];
        else
            return false;
    }
    
    
    
    /*
     * do billing from app call back
     */
    public function billingProcessApp()
    {
       // print_r($_POST);
       
        $token=$this->input->get('token');       
        $status=$this->input->get('response_code');       
        $transaction_id=$this->input->get('transaction_id');  
        $password=$this->input->get('password');  
        $email=$this->input->get('email');  
        
        $client_id=$this->input->get('client_id');       
        $package_id=$this->input->get('package_id');       
        
        $this->db->where('token',$token);
        $this->db->or_where('visa_trans_id',$transaction_id);
        $q=$this->db->count_all_results('billing_token');
        if($q>0)//already used token
        {
                echo json_encode(array('message_ar'=>'invalid token or transaction id.'
                ,'message_en'=>'invalid token or transaction id.','status'=>false,'need_login'=>false));
                     $log  = "invalid token ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                            "token=".$token.PHP_EOL.
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);            
            return;
        }
        
        
        $this->ApiModel->processBillingApp($token,$status,$transaction_id,$client_id,$package_id);
                echo json_encode(array('message_ar'=>'billing done'
                ,'message_en'=>'billing done','status'=>true,'need_login'=>false));
                $log  = "suvccess token ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                            "token=".$token.PHP_EOL.
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);
             return;
    }    
    
    
    
    /*
     * function service providers 
     */
    public function serviceProviders($lang='en')
    {
        $services=$this->ApiModel->services();
        $providers=$this->ApiModel->providersImages();
        $serviceProvider=$this->ApiModel->getServiceProvider();
     //   print_r($services);
      //  print_r($providers);
      //  return;
     //   print_r($serviceProvider);
        //$data['en']=$data['ar']=
        $data=array();
        $servicesAr=$services[0];
        $servicesEn=$services[1];
       // print_r($servicesAr);
        foreach($servicesAr as $key=>$value)
        {
            $aProviders=array();
            $aProvidersAr=$aProvidersEn=array();
            foreach($serviceProvider as $keyProvider=>$valueProvider)//serive with array of providers 
            {
                
                if($keyProvider==$key)//if loop in services mathcing key for the servic eprovider
                {
                    for($i=0;$i<sizeof($valueProvider);$i++)
                    {
                        $aProvidersAr[]=array('provider_name'=>$providers[0][$valueProvider[$i]][0],'provider_image'=>$providers[0][$valueProvider[$i]][1]);
                        
                        $aProvidersEn[]=array('provider_name'=>$providers[1][$valueProvider[$i]][0],'provider_image'=>$providers[1][$valueProvider[$i]][1]);
                    }
                }
            }
            if($lang=='en')
                $data[]=array('service'=>$servicesEn[$key],'service_providers'=>$aProvidersEn);
            else
                $data[]=array('service'=>$servicesAr[$key],'service_providers'=>$aProvidersAr);
            
        }

        echo json_encode($data);
    }
    
 /*
     * new logic
     */
    public function partners()
    {
        $providers=$this->ApiModel->partners();
         echo json_encode($providers);
    }
    /*
     * return language file
     */
    public function languagesForApp()
    {
        $page_id=$this->input->post('page_id');
        $array=array(
            array('page_one_en'=>lang('page_one_en'),
                'page_one_ar'=>lang('page_one_ar'),
                'page_two_en'=>lang('page_two_en'),
                'page_two_ar'=>lang('page_two_ar'),
                'verify_number_en'=>lang('verify_number_en'),
                'verify_number_ar'=>lang('verify_number_ar'),
                'verify_number_msg_en'=>lang('verify_number_msg_en'),
                'verify_number_msg_ar'=>lang('verify_number_msg_ar'),
                )
            
            
        );
       // print_r($this->lang);
        echo json_encode(array('lang_en'=>lang($page_id.'_en'),'lang_ar'=>lang($page_id.'_ar')));
        // echo json_encode(array($this->lang->language));
    }
    /*
     * partnert intro
     */
    public function partnersIntro($withPercentage=0)
    {
        
        $j=6;
        $a=array();
        for($i=1;$i<$j;$i++)
        {
            if($withPercentage==1)
                $a[]= array('id'=>(string)$i,'path'=>config('physicalPath').'img'.$i.'.svg','discount_rate'=>'25%');
            else
                $a[]= array('id'=>(string)$i,'path'=>config('physicalPath').'img'.$i.'.svg');
        }
        echo json_encode($a);
    }
    /*
     * register using name phone number 
     */
    public function regPhone()
    {
        $full_name=$this->input->post('full_name');
        $phone_number=$this->input->post('phone_number');
      //  echo $full_name.' '.$phone_number;


        if($full_name && $phone_number)
        {
            $checkPhoneAvailaibilty=$this->ApiModel->checkPhoneAvailaibilty($phone_number);

            if($checkPhoneAvailaibilty)
            {
                $pin=$this->generatePin(4);
                //echo $pin;

                $addedUser=$this->ApiModel->addUser($full_name,$phone_number,$pin);

                $secure_token=$this->secureToken($addedUser);
                
                if($addedUser>0)
                    echo json_encode(array('status'=>true,'pin'=>$pin,'client_id'=>$addedUser,'token'=>$secure_token));
                else
                    echo json_encode(array('status'=>false,'erro'=>'unable to add user'));
            }
            else
            {
                echo json_encode(array('status'=>false,'message_en'=>lang('phone_registred_en'),'message_ar'=>lang('phone_registred_ar')));
            }
        }
    }
    /*
     * verify user pin
     */
    public function verifyPin()
    {
        $client_id=$this->input->post('client_id');
        $token=$this->input->post('token');
        $pin=$this->input->post('pin');
       // echo $token;
        if($this->isAuthinticated($client_id,$token))
        {
            
            $verifyPin=$this->ApiModel->verifyPin($client_id,$pin);
            if($verifyPin>0)
            {
                if($verifyPin==100)//more than request
                {
                    echo json_encode(array('status'=>false,'message_en'=>lang('phone_registred_en'),'message_ar'=>lang('phone_registred_ar')));
                }
                else
                    
                    echo json_encode(array('status'=>true,'message_en'=>lang('phone_number_activated_en'),'message_ar'=>lang('phone_number_activated_ar')));
            }
            else
            {
                echo json_encode(array('status'=>false,'message_en'=>lang('verification_failed_en'),'message_ar'=>lang('verification_failed_ar')));
            }
        }
        else
            echo json_encode(array('status'=>false,'message_en'=>lang('error_happened_en'),'message_ar'=>lang('error_happened_ar')));
    }
    /*
     * resend code 
     */
    public function resendCode()
    {
        $client_id=$this->input->post('client_id');
        $token=$this->input->post('token');
        if($this->isAuthinticated($client_id,$token))
        {
            $pin=$this->generatePin(4);
            $checkTotalSent=$this->ApiModel->getTotalSentPin($client_id,$pin);
            if($checkTotalSent)
            {
                echo json_encode(array('status'=>true,'pin'=>$pin,'message_en'=>lang('activation_pin_sent_en'),'message_ar'=>lang('activation_pin_sent_ar')));
            }
            else
                echo json_encode(array('status'=>false,'message_en'=>lang('exceed_limit_en'),'message_ar'=>lang('exceed_limit_ar')));
        }
        else
            echo json_encode(array('status'=>false,'message_en'=>lang('error_happened_en'),'message_ar'=>lang('error_happened_ar')));
    }
    
    /*
     * set password for client
     */
    public function addClientPassword()
    {
        $client_id=$this->input->post('client_id');
        $token=$this->input->post('token');
        $password=$this->input->post('password');
        $verify_password=$this->input->post('verify_password');
        $passwordWithoutencruyption=$password;
        $verify_passwordwithoutencryption=$verify_password;
        if($this->isAuthinticated($client_id,$token))
        {
            $password=$this->encryptPassword($password);
            $verify_password=$this->encryptPassword($verify_password);
            if($passwordWithoutencruyption==$verify_passwordwithoutencryption)
            {
                $this->ApiModel->setClientPassword($client_id,$password);
                echo json_encode(array('status'=>true,'message_en'=>lang('password_set_en'),'message_ar'=>lang('password_set_ar')));
            }
            else {
                    echo json_encode(array('status'=>false,'message_en'=>lang('match_password_err_en'),'message_ar'=>lang('match_password_err_ar')));
            }
        }
        else
            echo json_encode(array('status'=>false,'message_en'=>lang('error_happened_en'),'message_ar'=>lang('error_happened_ar')));
    }
    /*
     * generate random pin
     */
    private function generatePin($lenth=4)
    {
        $a=array(1,2,3,4,5,6,7,8,9);
        $t='';
        for($i=0;$i<$lenth;$i++)
        {
            $t.=$a[rand(0,8)];
        }
        return $t;
    }
    
     public function encryptPassword($plain_text)
    {
        //$this->encryption->initialize(array('driver' => 'mcrypt'));
        //$plain_text = 'mohammed al shareif';
        //$ciphertext = $this->encryption->encrypt($plain_text);
       // return $ciphertext;
        // Outputs: This is a plain-text message!
       // echo $this->encryption->decrypt($ciphertext);
         return $this->secureToken($plain_text);
    }
    public function loginApi()
    {
        $phone_number=$this->input->post('phone_number');
        $password=$this->input->post('password');   
        if($phone_number && $password)
        {
            $getToken=$this->ApiModel->getTokenPassword($phone_number);
            //$password=$this->encryptPassword($password);
           // echo $password;
            if($this->verifyTokenNew($password, $getToken))
            {
                $loginProcess=$this->ApiModel->loginProcess($phone_number,$getToken);
               // print_r($loginProcess);
                if(($loginProcess))
                {
                    $secure_token=$this->secureToken($loginProcess->id);
                    echo json_encode(array('status'=>true,'client_id'=>$loginProcess->id,'token'=>$secure_token));
                }
               
            }
            else
            {
                echo json_encode(array('status'=>false,'message_en'=>'password '.lang('erro_phone_or_password_en'),'message_ar'=>lang('erro_phone_or_password_ar')));
            }             

        }
        else
        {
            echo json_encode(array('status'=>false,'message_en'=>lang('missing_phone_password_en'),'message_ar'=>lang('missing_phone_password_ar')));
        }
    }
    public function showHeaders()
    {
        $headers = [];
       foreach ($_SERVER as $name => $value)
       {
           if (substr($name, 0, 5) == 'HTTP_')
           {
               $headers[] = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5))))).' '.$value;
           }
       }
       $headers[]=$this->getRealIpAddr();
       $headers[]=$_SERVER['HTTP_X_REQUESTED_WITH'] ?$_SERVER['HTTP_X_REQUESTED_WITH']: null;
       echo json_encode($headers);
    }
    
    function getRealIpAddr(){
        if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
        // Check IP from internet.
        $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
        // Check IP is passed from proxy.
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
        // Get IP address from remote address.
        $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    
    /*
     * get client name from client id 
     */
    public function getClientInfo()
    {
        $client_id=$this->input->post('client_id');//service_provider value        
        $secure_token=$this->input->post('token');        
         if($this->isAuthinticated($client_id,$secure_token))
         {
             $client_details=$this->ApiModel->getClientDetails($client_id);
             echo json_encode($client_details);
         }else
         {
              echo json_encode(array('message_ar'=>'Try to Logout and login Again Again, Authintication Error.'
                ,'message_en'=>'Try to Logout and login Again Again, Authintication Error.','status'=>false,'need_login'=>true));
         }
    }

    public function unsubscribeClient()
    {
        $client_id=$this->input->post('client_id');//service_provider value        
        $secure_token=$this->input->post('token');        
         if($this->isAuthinticated($client_id,$secure_token))
         {
             $client_details=$this->ApiModel->unSubscribeClient($client_id);
                           echo json_encode(array('message_ar'=>'Unsubscribe done.'
                ,'message_en'=>'Unsubscribe done.','status'=>true,'need_login'=>false));
         }else
         {
              echo json_encode(array('message_ar'=>'Try to Logout and login Again Again, Authintication Error.'
                ,'message_en'=>'Try to Logout and login Again Again, Authintication Error.','status'=>false,'need_login'=>true));
         }
    }    

    /*
     * public function send qr
     * steps
     * check client subscription status 
     * 1 subscribed 2 not subscribed 3 expired 
     */
    public function qrValidationAndPayment()
    {
        $qr_value=$this->input->post('qr_value');//service_provider value        
        $amount=$this->input->post('amount');
        $v= explode('_', $qr_value);
        $client_id=$v[0];//$this->input->post('client_id');
        $secure_token=$v[1];;//$this->input->post('token');
        $payment_type=(int)$this->input->post('payment_type');   
        //to be get from database after link cliewnt account with provider 
        //- provider should be registered as client 
        //then we connect the provider with client ids 
        //provider id 
        //return values form db with login as provider 
        $service_provider_client_id=$this->input->post('service_provider');
        $secure_token_provider=$this->input->post('secure_token_provider');
        if($this->isAuthinticated($client_id,$secure_token) && $amount>0 && $this->isAuthinticated($service_provider_client_id,$secure_token_provider))
        {
            $providerDEtails=$this->ApiModel->getProviderIdFromClient($service_provider_client_id);
            $service_provider=$providerDEtails[0];
            $provider_name=$providerDEtails[1];
            if($service_provider==-1)//error in data 
            {
                 echo json_encode(array('message_ar'=>lang('provider_id_error_ar')
                ,'message_en'=>lang('provider_id_error_en'),'status'=>false,'need_login'=>true));                
                return;
            }
            $subscription=$this->isSubscribed($client_id);//get subscription result 
            switch ($subscription)
            {
              case 1://active subscriber
                    $this->manageTransactionPayments($client_id,$amount,$service_provider,1,$provider_name);
                  break;
              case 0://not subscribed
                  $this->manageTransactionPayments($client_id,$amount,$service_provider,0,$provider_name);
                  break;
              case 2: 
                  $this->manageTransactionPayments($client_id,$amount,$service_provider,2,$provider_name);
                  break;
              default :
                  return -1;
                  break;
            }
            return;
        }
        else
        {
            echo json_encode(array('message_ar'=>'Try to Logout and login Again Again, Authintication Error.'
                ,'message_en'=>'Try to Logout and login Again Again, Authintication Error.','status'=>false,'need_login'=>true));
        }
        
    }    
    /*
     * billing steps 
     * 
     */
    private function manageTransactionPayments($client_id,$amount,$service_provider,$subscription,$provider_name='')
    {
        $discountRate= config('non_subscription_discount_rate');
        if($subscription==1)
            $discountRate= config('subscription_discount_rate');
        $discountRate=($discountRate/100);

        /*
         * just need to insert ppending transaction 
         */
        $this->ApiModel->insertPendingTRansactionToManage($client_id,$amount,0,2,0,0,0,$discountRate,$service_provider,$provider_name);//2 menas deduction
        
         echo json_encode(array('message_ar'=>lang('transaction_sent_to_client_to_pay_ar'),'message_en'=>lang('transaction_sent_to_client_to_pay_en'),'status'=>true,'need_login'=>false));         
        return;
        

       
    }
    
    /*
     * get clinet pending transaction to be proccessed 
     * 
     */
    public function getClientTransaction()
    {
        $client_id=$this->input->post('client_id');
        $secure_token=$this->input->post('token');
        $status=(int)$this->input->post('status');
        $offset=(int)$this->input->post('offset');
        if($this->isAuthinticated($client_id,$secure_token))
        {
            echo $this->ApiModel->getClientTransaction($client_id, $status,$offset);
            return;
        }
        else
        {
            echo json_encode(array('message_ar'=>lang('authintication_erro_ar')
                ,'message_en'=>lang('authintication_erro_en'),'status'=>false,'need_login'=>true));
        }
    }
    /*
     * change transaction status 
     * request come from client to complete process 
     * 
     */
    public function completeTransactionProcess()
    {
        
        $client_id=$this->input->post('client_id');
        $secure_token=$this->input->post('token');
        $status=(int)$this->input->post('status'); 
        $transaction_id=(int)$this->input->post('transaction_id');
        $payment_type=(int)$this->input->post('payment_type');
        if($this->isAuthinticated($client_id,$secure_token))
        {
            
            //close transactions by any other actions than approve
            if($status!=1)
            {
                $this->ApiModel->completeTransactionProcessforNonApproved($client_id,$transaction_id,$status);
                //echo'ss';
              echo json_encode(array('message_ar'=>lang('status_changed_'.$status.'_ar'),'message_en'=>lang('status_changed_'.$status.'_en'),'status'=>true,'need_login'=>false));
                return;
            }
        /*
                 * below process once the client complete the steps 
                 */
                if($payment_type==3)//from wallet
                {

                    $walletResult=$this->ApiModel->transactionProcessFinalizeWallet($client_id,$transaction_id,$status);//2 menas deduction
                    if($walletResult==1)
                    {
                    echo json_encode(array('message_ar'=>'done'
                                   ,'message_en'=>'done','status'=>true,'need_login'=>false)); 
                    return;                      
                    }
                    else
                        if($walletResult==2)
                        {
                                   echo json_encode(array('message_ar'=> lang('insufficiant_balance_en')
                                   ,'message_en'=>lang('insufficiant_balance_en'),'status'=>true,'need_login'=>false)); 
                                   return;
                        }
                        else
                        {
                                                            echo json_encode(array('message_ar'=> lang('insufficiant_balance_en')
                                   ,'message_en'=>lang('insufficiant_balance_en'),'status'=>true,'need_login'=>false));
                                                            return;
                        }
                     
        
                }

                if($payment_type==2)//visa
                {

                }
                if($payment_type==3)//cash
                {

                }        
        }
        
    }
    /*
     * return payment types 
     * as 1 cash 2 visa 
     */
    public function subscriptionPaymentGateway()
    {
        /*$types=config('payment_types');
        $a=array();
        foreach($types as $t)
        {
            $a[]=lang('payment_'.$t);
        }*/
        $a=array(array('payment_type'=>1,'type'=>'Cash','text'=>'cash text cash text cash text cash text cash text cash text cash text cash text cash text cash text cash text cash text cash text ','img'=>config('physicalPath').'cash.svg'),array('payment_type'=>2,'type'=>'visa','text'=>'cvisash text cvisash text cvisash text cvisash text cvisash text cvisash text cvisash text cvisash text cvisash text cvisash text cvisash text cvisash text cvisash text cvisash text cvisash text cvisash text','img'=>config('physicalPath').'creditcard.svg'));
        echo json_encode($a);
    }
    /*
     * subscripotion process 
     * payment now from wallet
     * payment_types
     */
    public function clientSubscriptionProcess()
    {
        //do payment from cash in wallet till solve visa billing 
        $client_id=$this->input->post('client_id');
        $secure_token=$this->input->post('token');               
        $payment_type=(int)$this->input->post('payment_type');   
        if($this->isAuthinticated($client_id,$secure_token))    
        {
            $subscriptionStatus=$this->isSubscribed($client_id);//
            if($subscriptionStatus==1)
            {
                echo json_encode(array('message_ar'=>lang('already_subscribed_ar') ,'message_en'=>lang('already_subscribed'),'status'=>true,'need_login'=>false)); 
                return;                
            }
            else
            {
                if($payment_type===1)//wallet
                {
                    //sunubscribe to defaulkt package
                    $result=$this->ApiModel->subscriptionTransaction($client_id,$payment_type,$subscriptionStatus);
                    switch ($result)
                    {
                        case 1:
                            echo json_encode(array('message_ar'=>lang('subscription_done_ar') ,'message_en'=>lang('subscription_done_en'),'status'=>true,'need_login'=>false)); 
                            break;
                        case -1:
                              echo json_encode(array('message_ar'=>lang('insufficiant_balance_ar') ,'message_en'=>lang('insufficiant_balance_en'),'status'=>true,'need_login'=>false)); 
                            break;
                        case -3:
                              echo json_encode(array('message_ar'=>lang('error_found_desc') ,'message_en'=>lang('error_found_desc'),'status'=>false,'need_login'=>true)); 
                            break;    
                        case -2:
                              echo json_encode(array('message_ar'=>lang('default_package_not_defined_ar') ,'message_en'=>lang('default_package_not_defined_en'),'status'=>true,'need_login'=>false)); 
                            break;                         
                        
                    }
                }
                else
                if($payment_type===2)//visa
                {
                    echo 'no supported now';
                }                    
                else
                if($payment_type===3)//cash
                {
                    echo 'no supported now';
                }
            }
        }
    }
    /*
     * return details transaction for 
     */
    public function getTransactionDetails()
    {
        $client_id=$this->input->post('client_id');
        $secure_token=$this->input->post('token');               
        $transaction_id=(int)$this->input->post('transaction_id');   
        if($this->isAuthinticated($client_id,$secure_token))   
        {
            $transactionDetails=$this->ApiModel->getTransactionDtails($client_id,$transaction_id);//2 menas deduction
            echo json_encode($transactionDetails);
        }
        else
        {
                        echo json_encode(array('message_ar'=>'Try to Logout and login Again Again, Authintication Error.'
                ,'message_en'=>'Try to Logout and login Again Again, Authintication Error.','status'=>false,'need_login'=>true));
        }
    }
    /*
     * we need to set the subscription process 
     * logic to check subscription process 
     */
    private function isSubscribed($client_id)
    {
        $subscriptionStatus=$this->ApiModel->getSubscriptionStatus($client_id);
        return $subscriptionStatus;
    }
    /*
     * generate secure token
     */
    private function secureToken($client_id)
    {
        return $this->secureTokenNew($client_id);
    }
    /*
     * new security alogrithm
     */
    private function secureTokenNew($client_id)
    {
        $hash= password_hash($client_id,PASSWORD_BCRYPT );
        $hash= str_replace('\\', '', $hash);
        return $hash;
    }
    
    private function verifyTokenNew($client_id,$hash)
    {
        $hash= str_replace('\\', '', $hash);
        $verifyPassword= password_verify($client_id, $hash);
        if($verifyPassword)
            return true;
        else
            return false;
    }
    
    private function isAuthinticated($client_id,$secure_token)
    {
       // echo $secure_token.'<br/>';;
        $secure_token= str_replace('\\', '', $secure_token);
       // echo $secure_token.'<br/>';;
         if($this->verifyTokenNew($client_id,$secure_token))
             return true;
         return false;
    }    
}

