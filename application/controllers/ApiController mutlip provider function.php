<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApiController
 *
 * @author shareifhome
 */
class ApiController extends CI_Controller {
    //put your code here
    
    public function __construct() {
        parent::__construct();
        
        $this->load->database();
        
    }
    public function _remap($method, $params = array())
    {

        
        if (method_exists($this, $method))
        {
                return call_user_func_array(array($this, $method), $params);
        }
        show_404();
    } 
    
    /*
     * login register with same functiuon 
     */
    public function loginResgister()
    {
        $user_name=$this->input->get('user_name');
        $user_email=$this->input->get('user_email');
        $user_socail_id=$this->input->get('user_social_id');
        $type=$this->input->get('user_type');//facebook or gmail 1,2
        $device_udid=$this->input->get('device_udid');
        $device_token=$this->input->get('device_token');
        if($type==0)
            $type=1;
        $whereRegisterArray=array('social_id'=>$user_socail_id,'social_type'=>$type);
        $existedUser=$this->ApiModel->checkRegisterUser($whereRegisterArray,$device_token);
        $mobileType=new Mobile_Detect();
        $deviceType='Android';
        if($mobileType->is('AndroidOS'))
            $deviceType='Android';
        if($mobileType->is('iOS'))
            $deviceType='Ios';
       // echo $deviceType;
        if($existedUser>0)//gather user info
        {
            //echo $existedUser;
           $info=$this->gatherInfoForClient($existedUser);
           //==========log
           $log  = "already registerd User: ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                   "client_id=".$info['client_id'].PHP_EOL.
                    "client_info=".json_encode($info).PHP_EOL.
            "-------------------------".PHP_EOL;
                //Save string to log, use FILE_APPEND to append.
               logFile($log);
           //==========
        }else//new user and return info
        {
            $insertUser=$this->ApiModel->createUser($user_name,$user_email,$user_socail_id,$type,$device_token,$deviceType);
            $info=$this->gatherInfoForClient($insertUser);
            
           $log  = "new User: ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                   "client_id=".$info['client_id'].PHP_EOL.
                   "client_info=".json_encode($info).PHP_EOL.
            "-------------------------".PHP_EOL;
                //Save string to log, use FILE_APPEND to append.
               logFile($log);            
        }
        
        $info['status']=true;
        $info['need_login']=false;
        echo json_encode($info);
    }
    /*
     * returnb required info for users 
     */
    private function gatherInfoForClient($client_id)
    {
        $data['client_id']=$client_id;
        $data['balances_ar']=$data['balances_en']='';
        $data['secure_token']=$this->secureToken($client_id);//pass secure token to be used in APIs
        //return subscription statu
        $getSubscriptionData=$this->ApiModel->getSubscriptionData($data['client_id']);
        $data['no_subscription_ar']='';
        $data['no_subscription_en']='';        
        if($getSubscriptionData)
        {
            
            $data['package_id']=$getSubscriptionData['package_id'];
            $packageDetails=$this->ApiModel->getPackages($data['package_id'])[0];
            $data['subscription_date']=date('Y-m-d',$getSubscriptionData['subscription_date']);
            $data['expiry_date']=date('Y-m-d',$getSubscriptionData['expiry_date']);
            $data['is_active_subscriber']=( ($getSubscriptionData['expiry_date']>time())?true:FALSE);
            $data['is_subscribe']=true;
            $data['payment_method']='Visa Card';
            if($packageDetails)
            {
                foreach($packageDetails as $key=>$value)
                {
                    $data[$key]=$value;
                }
            }
            //=========get client balances 
            
            if($data['service_list'])
            {
                $balances=$this->ApiModel->buildBalances($data['client_id'],$data['service_list']);
                $data['balances_ar']=$balances[0];
                $data['balances_en']=$balances[1];
            }
        }
        else
        {
            $data['package_id']=0;
            $data['package_ar']='';
            $data['package_en']='';
            $data['subscription_date']=0;
            $data['expiry_date']=0;
            $data['is_subscribe']=false;
            $data['is_active_subscriber']=false;
            $data['desc_en']='';
            $data['desc_ar']=$data['service_list']=$data['package_price']=$data['is_offer']=$data['package_period_days']=$data['package_period_label_en']=$data['package_period_label_ar']=$data['servicesArLabels']=$data['servicesEnLabels']='';
            $data['no_subscription_ar']=lang('no_subscription_found_ar');
            $data['no_subscription_en']=lang('no_subscription_found');
            $data['payment_method']='';
        }
        return $data;
    }
    
    /*
     * public function send qr
     */
    public function sendQr()
    {
        $client_id= $this->input->get('client_id');
        $secure_token=$this->input->get('secure_token');
        $qr_value=$this->input->get('qr_value');//service_provider value
        if($this->isAuthinticated($client_id,$secure_token) && $qr_value)
        {
            $qrValues=explode('_',$qr_value);
            $service_id=(int)$qrValues[0];
            $provider_id=(int)$qrValues[1];
            //echo $client_id.'==>'.$service_id.'==>'.$provider_id;
            $gatherInfo=$this->gatherInfoForClient($client_id);
            
            if($gatherInfo['is_subscribe']==FALSE)// client not subscribed 
            {
                echo json_encode(array('message_ar'=>lang('sub_first_ar'),
                                            'message_en'=>lang('sub_first'),'status'=>false,'need_login'=>false));
                $log  = "not subscribed client: ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                                   "client_id=".$gatherInfo['client_id'].PHP_EOL.
                                    "service_id=".$service_id.PHP_EOL.
                                   "client_info=".json_encode($gatherInfo).PHP_EOL.
                            "-------------------------".PHP_EOL;
                                //Save string to log, use FILE_APPEND to append.
                               logFile($log,'_'.__FUNCTION__);                            
                return;
            }
            if($gatherInfo['is_active_subscriber']==FALSE)// client not subscribed 
            {
                echo json_encode(array('message_ar'=>lang('renew_subscription_ar'),
                                            'message_en'=>lang('renew_subscription'),'status'=>false,'need_login'=>false));
            $log  = "expired subscription client: ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                                              "client_id=".$gatherInfo['client_id'].PHP_EOL.
                                               "service_id=".$service_id.PHP_EOL.
                                              "client_info=".json_encode($gatherInfo).PHP_EOL.
                                       "-------------------------".PHP_EOL;
                                           //Save string to log, use FILE_APPEND to append.
                                           logFile($log,'_'.__FUNCTION__);                          
                return;
            }
            if(!isset($gatherInfo['service_'.$service_id]))
            {
                echo json_encode(array('message_ar'=>lang('service_not_in_package_ar'),
                                            'message_en'=>lang('service_not_in_package'),'status'=>false,'need_login'=>false));
            $log  = "client not in service: ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                                              "client_id=".$gatherInfo['client_id'].PHP_EOL.
                                               "service_id=".$service_id.PHP_EOL.
                                              "client_info=".json_encode($gatherInfo).PHP_EOL.
                                       "-------------------------".PHP_EOL;
                                           //Save string to log, use FILE_APPEND to append.
                                           logFile($log,'_'.__FUNCTION__);                   
                return;                
            }
            $getCurrentBalance=$this->getCurrentBalance($client_id,$service_id);
            if($getCurrentBalance<=0)
            {
                echo json_encode(array('message_ar'=>lang('balance_consumed_ar'),
                                            'message_en'=>lang('balance_consumed'),'status'=>false,'need_login'=>false));
            $log  = "client consumed balance for service ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                                              "client_id=".$gatherInfo['client_id'].PHP_EOL.
                                               "service_id=".$service_id.PHP_EOL.
                                              "client_info=".json_encode($gatherInfo).PHP_EOL.
                                       "-------------------------".PHP_EOL;
                                           //Save string to log, use FILE_APPEND to append.
                                           logFile($log,'_'.__FUNCTION__);                  
                return;                
            }
            
            //------ deduction process
          // print_r($gatherInfo);
            $this->updateClientBalanceTable($client_id,$service_id);
            $this->logProviderClientData($provider_id,$client_id,$service_id);
            $remainingBalance=$getCurrentBalance-1;//remainig balance
            $serviceLabels=$this->ApiModel->services($service_id);
           // print_r($serviceLabels);
            $msgAr=sprintf(lang('ticket_used_ar'), $serviceLabels[0][$service_id]);
            //echo $msgAr;
            //return;
            $msgEn=sprintf(lang('ticket_used'), $serviceLabels[1][$service_id]);
            $balanceAr=sprintf(lang('your_ticket_balance_ar'),$remainingBalance);
            $balanceEn=sprintf(lang('your_ticket_balance'),$remainingBalance);
            $validAr=sprintf(lang('valid_till_ar'),$gatherInfo['expiry_date']);
            $validEn=sprintf(lang('valid_till'),$gatherInfo['expiry_date']);
             echo json_encode(array('message_ar'=> $msgAr,'message_en'=>$msgEn,'status'=>true,'need_login'=>false,
                 'ticket_balance_ar'=> $balanceAr,
                 'ticket_balance'=> $balanceEn,
                 'valid_till_ar'=> $validAr,
                 'valid_till'=> $validEn
                 ));
             //push msg
                         $log  = "client got the service ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                                              "client_id=".$gatherInfo['client_id'].PHP_EOL.
                                               "service_id=".$service_id.PHP_EOL.
                                              "client_info=".json_encode($gatherInfo).PHP_EOL.
                                       "-------------------------".PHP_EOL;
                                           //Save string to log, use FILE_APPEND to append.
                                           logFile($log,'_'.__FUNCTION__);   
                                           $this->pushMessage($client_id, __FUNCTION__);
             return;
        }
        else
        {
            echo json_encode(array('message_ar'=>lang('authintication_error_ar')
                ,'message_en'=>lang('authintication_error'),'status'=>false,'need_login'=>true));
        }
        
    }
    /*
     * getCurrentBalance
     */
    private function getCurrentBalance($client_id,$service_id)
    {
        $this->db->where(array('client_id'=>$client_id,'service_id'=>$service_id));
        $q=$this->db->get('client_service_balances')->row_array();
        return (int)$q['balance'];
    }
    /*
     * updateClientBalanceTable
     */
    private function updateClientBalanceTable($client_id,$service_id)
    {
        $this->db->where(array('client_id'=>$client_id,'service_id'=>$service_id));
        $this->db->set('balance','balance-1',false);
        $this->db->update('client_service_balances');
    }

    
    private function logProviderClientData($provider_id,$client_id,$service_id)
    {
        $data=array('provider_id'=>$provider_id,'client_id'=>$client_id,'service_id'=>$service_id,'paid'=>0,'datetime'=>time());
        $this->db->insert('provider_logs',$data);
        $data=array('provider_id'=>$provider_id,'client_id'=>$client_id,'service_id'=>$service_id,'datetime'=>time());
        $this->db->insert('client_logs',$data);
    }
    /*
     * return available packages on the system 
     */
    public function packageList()
    {
        echo json_encode($this->ApiModel->getPackages('',1));
    }
    /*
     * generate secure token
     */
    private function secureToken($client_id)
    {
        return md5(md5('shareifTxt'.$client_id));
    }
    /*
     * billing token generator
     */
    public function generateToken()
    {
       
        $client_id= $this->input->get('client_id');
        $package_id= $this->input->get('package_id');
        $secure_token=$this->input->get('secure_token');
        if($this->isAuthinticated($client_id,$secure_token) && (($package_id))>0)
        {
            $gatherInfo=$this->gatherInfoForClient($client_id);
            if($gatherInfo['is_active_subscriber']==TRUE || $gatherInfo['is_subscribe']==TRUE)// client subscribed 
            {
                echo json_encode(array('message_ar'=>'Please unsub from your package before',
                                            'message_en'=>'Please unsub from your package before','status'=>false,'need_login'=>false));
                $log  = "client already sub ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                            "client_id=".$gatherInfo['client_id'].PHP_EOL.
                             "package_id=".$package_id.PHP_EOL.
                            "client_info=".json_encode($gatherInfo).PHP_EOL.
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);
                
                return;
            }
            //---check if user have pending process 
            $getPendingBilling=$this->ApiModel->getPendingBillings($client_id);
            if($getPendingBilling>0)//if the client have pending billings
            {
                echo json_encode(array('message_ar'=>'you have pending process please wait',
                                            'message_en'=>'you have pending process please wait','status'=>false,'need_login'=>false));
                        $log  = "client pending sub ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                            "client_id=".$gatherInfo['client_id'].PHP_EOL.
                             "package_id=".$package_id.PHP_EOL.
                            "client_info=".json_encode($gatherInfo).PHP_EOL.
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);
                return;
            }
            $token= md5(uniqid(rand().$client_id.$package_id, true));
            $data=array('client_id'=>$client_id,'package_id'=>$package_id,'token'=>$token,'parsed'=>0,'billing_status'=>0,'datetime'=>time());
            $this->db->insert('billing_token',$data);
            echo json_encode(array('message_ar'=>'token generated'
                ,'message_en'=>'token generated','status'=>true,'token'=>$token));
                    $log  = "client generet token sub ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                            "client_id=".$gatherInfo['client_id'].PHP_EOL.
                             "package_id=".$package_id.PHP_EOL.
                            "client_info=".json_encode($gatherInfo).PHP_EOL.
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);
        }
        else
        {
            echo json_encode(array('message_ar'=>'Try to Logout and login Again Again, Authintication Error.'
                ,'message_en'=>'Try to Logout and login Again Again, Authintication Error.','status'=>false,'need_login'=>true));
        }
        
    }
    
    private function isAuthinticated($client_id,$secure_token)
    {
         if($this->secureToken($client_id)==$secure_token)
             return true;
         return false;
    }
    /*
     * get billing confirmation
     */
    public function confirmBilling()
    {
       // print_r($_POST);
        $token=$this->input->get('token');       
        $status=$this->input->get('status');       
        $payment_reference=$this->input->post('payment_reference');
        $checkValidity=$this->ApiModel->checkTokenValidity($token);
        if($checkValidity==0)        
        {
             echo json_encode(array('message_ar'=>'invalid token.'
                ,'message_en'=>'invalid token.','status'=>false,'need_login'=>false));
                     $log  = "invalid token ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                            "token=".$token.PHP_EOL.
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);
             return;
        }
        $this->ApiModel->processBilling($token,$status,$payment_reference);
                echo json_encode(array('message_ar'=>lang('billing_done_ar')
                ,'message_en'=>lang('billing_done'),'status'=>true,'need_login'=>false));
                $log  = "suvccess token ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                            "token=".$token.PHP_EOL.
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);
             return;
    }
    /*
     * unsub from package
     */
    public function cancelSubscription()
    {
        $client_id= $this->input->get('client_id');
        $package_id= $this->input->get('package_id');
        $secure_token=$this->input->get('secure_token');
        if($this->isAuthinticated($client_id,$secure_token) && (($package_id))>0)
        {
            $this->ApiModel->cancelSubscription($client_id,$package_id);
             $this->pushMessage($client_id,__FUNCTION__);
             echo json_encode(array('message_ar'=>lang('unsub_message_ar')
                ,'message_en'=>lang('unsub_message'),'status'=>true,'need_login'=>false));
             $log  = "cancel sub  ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                            "client_id=".$client_id.PHP_EOL.
                       "package_id=".$package_id.PHP_EOL.
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);
             //push
           
        }
        else
        {
             echo json_encode(array('message_ar'=>'Try to Logout and login Again Again, Authintication Error.'
                ,'message_en'=>'Try to Logout and login Again Again, Authintication Error.','status'=>false,'need_login'=>true));
        }
    }
    /*
     * public function cancel pending billing tokens
     */
    public function cancelPendingBillings()
    {
        $client_id= $this->input->get('client_id');
        $secure_token=$this->input->get('secure_token');
        if($this->isAuthinticated($client_id,$secure_token))
        {
            $this->ApiModel->cancelPendingBilling($client_id);
             echo json_encode(array('message_ar'=>'successfuly canceled.'
                ,'message_en'=>'successfuly canceled.','status'=>true,'need_login'=>false));
             $log  = "cancel sub  ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                            "client_id=".$client_id.PHP_EOL.
                       
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);
             //push
        }
        else
        {
             echo json_encode(array('message_ar'=>lang('authintication_error_ar')
                ,'message_en'=>lang('authintication_error'),'status'=>false,'need_login'=>true));
        }
    }
    /*
     * public function used tickect 
     */
    public function usedTicketHistory()
    {
        $client_id= $this->input->get('client_id');
        $secure_token=$this->input->get('secure_token');
        if($this->isAuthinticated($client_id,$secure_token))
        {
            $history['list']=$this->ApiModel->getClientLogs($client_id);
            
            if(sizeof($history['list'])>0)
            {
                $history['info_ar']=array('message_ar'=>lang('used_ticket_title_ar'),'status'=>true,'need_login'=>false);
                $history['info_en']=array('message_en'=>lang('used_ticket_title'),'status'=>true,'need_login'=>false);

                
                echo json_encode(($history));
            }
            else
            {
//                echo json_encode(array('list'=>array(),'message_ar'=>'no records found'
//                ,'message_en'=>'sno records found','status'=>true,'need_login'=>false));
                echo json_encode(array('list'=>array(),'info_ar'=>array('message_ar'=>lang('no_data_found_ar'),'status'=>true,'need_login'=>false)
                ,'info_en'=>array('message_en'=>lang('no_data_found'),'status'=>true,'need_login'=>false)));
            }
             
             $log  = "showing ticket history  ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                            "client_id=".$client_id.PHP_EOL.
                       
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);
             //push
        }
        else
        {
            echo json_encode(array('message_ar'=>lang('authintication_error_ar')
                ,'message_en'=>lang('authintication_error'),'status'=>false,'need_login'=>true));;
        }
    }
    /*
     * 
     */
    public function privacyPolicyAgrement()
    {
        echo json_encode($this->ApiModel->privacyPolicyAgrement());
    }
    /*
     * 
     * encrypt and decrpt values
     */
    public function encryptVisaValue()
    {
        $this->encryption->initialize(array('driver' => 'mcrypt'));
        $plain_text = 'mohammed al shareif';
        $ciphertext = $this->encryption->encrypt($plain_text);
        ECHO $ciphertext;
        // Outputs: This is a plain-text message!
       // echo $this->encryption->decrypt($ciphertext);
    }

    /*
     * get updated user data 
     */
    public function updateUserData()
    {
         $client_id= $this->input->get('client_id');
        $secure_token=$this->input->get('secure_token');
        if($this->isAuthinticated($client_id,$secure_token))
        {
            $info=$this->gatherInfoForClient($client_id);
            
            echo json_encode($info);
        }
        else
        {
                        echo json_encode(array('message_ar'=>lang('authintication_error_ar')
                ,'message_en'=>lang('authintication_error'),'status'=>false,'need_login'=>true));;;
        }
        
    }
    
    public function getClientBalances()
    {
        $client_id= $this->input->get('client_id');
        $secure_token=$this->input->get('secure_token');
        if($this->isAuthinticated($client_id,$secure_token))
        {
            $data['client_id']=$client_id;
            $data['balances']=array();
            $data['secure_token']=$this->secureToken($client_id);//pass secure token to be used in APIs
            //return subscription statu
            $getSubscriptionData=$this->ApiModel->getSubscriptionData($data['client_id']);
            if($getSubscriptionData)
            {

                $data['package_id']=$getSubscriptionData['package_id'];
                $packageDetails=$this->ApiModel->getPackages($data['package_id'])[0];
                $data['subscription_date']=date('Y-m-d',$getSubscriptionData['subscription_date']);
                $data['expiry_date']=date('Y-m-d',$getSubscriptionData['expiry_date']);
                $data['is_active_subscriber']=( ($getSubscriptionData['expiry_date']>time())?true:FALSE);
                $data['is_subscribe']=true;
                if($packageDetails)
                {
                    foreach($packageDetails as $key=>$value)
                    {
                        $data[$key]=$value;
                    }
                }
                //=========get client balances 

                if($data['service_list'] && $data['is_active_subscriber'])
                {
                    $data['balances']=$this->ApiModel->buildBalances($data['client_id'],$data['service_list']);
                }
                
            }
            else
            {
                

            }
        }
        else
        {
                        echo json_encode(array('message_ar'=>lang('authintication_error_ar')
                ,'message_en'=>lang('authintication_error'),'status'=>false,'need_login'=>true));;
             return;
        }
            echo json_encode($data['balances']);
    }
    /*
     * return required info for mechinate 
     */
    
    public function getMerchintInfio() {
        $client_id= $this->input->get('client_id');
        $secure_token=$this->input->get('secure_token');
        if($this->isAuthinticated($client_id,$secure_token))
        {
            $transaction_title='Subscription Process';
            $curencyCode='USD';
            $customer_email=$client_id.'@scan_parks.info';
            $dbEmail=$this->getClientEmail($client_id);
            if($dbEmail)
               $customer_email=$dbEmail;
            $customer_phone_number='9627'.$client_id;
             $fields=array('order_id'=>(string)rand(50,350),'transaction_title'=>$transaction_title,'currency_code'=>$curencyCode,'customer_email'=>$customer_email,
                 'customer_phone_number'=>$customer_phone_number,'merchant_email'=>'mohdsh85@gmail.com','secret_key'=>'HQLxsmlEsCk2lXGsAskdVrxBbAqxcUITLurdRNSjqclLa2dKuxYpYw1a6sHjmqd7UFlLsd4TqqEAsRlXpkmnQoyWRffeFtmA8IlB');
             echo json_encode($fields);
        }
           else
        {
                        echo json_encode(array('message_ar'=>lang('authintication_error_ar')
                ,'message_en'=>lang('authintication_error'),'status'=>false,'need_login'=>true));;;
        }
    }
    
    /*
     * get client email
     */
    private function getClientEmail($client_id)
    {
        $this->db->where('id',$client_id);
        $q=$this->db->get('clients')->result_array()[0];
       // print_r($q);
        if($q['client_email']!='')
            return $q['client_email'];
        else
            return false;
    }
    
    
    
    /*
     * do billing from app call back
     */
    public function billingProcessApp()
    {
       // print_r($_POST);
       
        $token=$this->input->get('token');       
        $status=$this->input->get('response_code');       
        $transaction_id=$this->input->get('transaction_id');  
        $password=$this->input->get('password');  
        $email=$this->input->get('email');  
        
        $client_id=$this->input->get('client_id');       
        $package_id=$this->input->get('package_id');       
        
        $this->db->where('token',$token);
        $this->db->or_where('visa_trans_id',$transaction_id);
        $q=$this->db->count_all_results('billing_token');
        if($q>0)//already used token
        {
                echo json_encode(array('message_ar'=>'invalid token or transaction id.'
                ,'message_en'=>'invalid token or transaction id.','status'=>false,'need_login'=>false));
                     $log  = "invalid token ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                            "token=".$token.PHP_EOL.
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);            
            return;
        }
        
        
        $result= $this->ApiModel->processBillingApp($token,$status,$transaction_id,$client_id,$package_id);
                if($result==100)
                    $this->pushMessage ($client_id,__FUNCTION__);
                echo json_encode(array('message_ar'=>lang('billing_done_ar')
                ,'message_en'=>lang('billing_done'),'status'=>true,'need_login'=>false));
                $log  = "suvccess token ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                        "vis billing clien id =".$client_id.PHP_EOL.
                        "result=".$result.PHP_EOL.
                            "token=".$token.PHP_EOL.
                     "-------------------------".PHP_EOL;
                         //Save string to log, use FILE_APPEND to append.
                         logFile($log,'_'.__FUNCTION__);
             return;
    }    
    
    
    
    /*
     * function service providers 
     */
    public function serviceProviders($lang='en')
    {
        $services=$this->ApiModel->services();
        $providers=$this->ApiModel->providersImages();
        $serviceProvider=$this->ApiModel->getServiceProvider();
     //   print_r($services);
      //  print_r($providers);
      //  return;
     //   print_r($serviceProvider);
        //$data['en']=$data['ar']=
        $data=array();
        $servicesAr=$services[0];
        $servicesEn=$services[1];
       // print_r($servicesAr);
        
        foreach($servicesAr as $key=>$value)
        {
            $k=1;
            $aProviders=array();
            $aProvidersAr=$aProvidersEn=array();
            foreach($serviceProvider as $keyProvider=>$valueProvider)//serive with array of providers 
            {
                $haveNext=false;
                if($keyProvider==$key)//if loop in services mathcing key for the servic eprovider
                {
                    for($i=0;$i<sizeof($valueProvider);$i++)
                    {
                        $aProvidersAr[]=array('provider_name'=>$providers[0][$valueProvider[$i]][0],'provider_image'=>$providers[0][$valueProvider[$i]][1]);
                        
                        $aProvidersEn[]=array('provider_name'=>$providers[1][$valueProvider[$i]][0],'provider_image'=>$providers[1][$valueProvider[$i]][1]);
                        if($k==1)
                        {
                           // $haveNext=true;
                            //break;
                        }
                            
                        $k++;
                    }
                }
            }
            if($lang=='en')
                $data[]=array('service_id'=>$key,'service'=>$servicesEn[$key],'have_next'=>$haveNext,'service_providers'=>$aProvidersEn);
            else
                $data[]=array('service_id'=>$key,'service'=>$servicesAr[$key],'have_next'=>$haveNext,'service_providers'=>$aProvidersAr);
            
        }

        echo json_encode($data);
    }
    
    
    
    public function serviceProvidersFull($service_id,$lang='en')
    {
        $services=$this->ApiModel->services($service_id);
        $providers=$this->ApiModel->providersImages();
        $serviceProvider=$this->ApiModel->getServiceProvider();
     //   print_r($services);
      //  print_r($providers);
      //  return;
     //   print_r($serviceProvider);
        //$data['en']=$data['ar']=
        $data=array();
        $servicesAr=$services[0];
        $servicesEn=$services[1];
       // print_r($servicesAr);
        
        foreach($servicesAr as $key=>$value)
        {

            $aProviders=array();
            $aProvidersAr=$aProvidersEn=array();
            foreach($serviceProvider as $keyProvider=>$valueProvider)//serive with array of providers 
            {
                $haveNext=false;
                if($keyProvider==$key)//if loop in services mathcing key for the servic eprovider
                {
                    for($i=0;$i<sizeof($valueProvider);$i++)
                    {
                        $aProvidersAr[]=array('provider_name'=>$providers[0][$valueProvider[$i]][0],'provider_image'=>$providers[0][$valueProvider[$i]][1]);
                        
                        $aProvidersEn[]=array('provider_name'=>$providers[1][$valueProvider[$i]][0],'provider_image'=>$providers[1][$valueProvider[$i]][1]);
      
                    }
                }
            }
            if($lang=='en')
                $data[]=array('service_id'=>$key,'service'=>$servicesEn[$key],'have_next'=>$haveNext,'service_providers'=>$aProvidersEn);
            else
                $data[]=array('service_id'=>$key,'service'=>$servicesAr[$key],'have_next'=>$haveNext,'service_providers'=>$aProvidersAr);
            
        }

        echo json_encode($data);
    }
 
    
    
    public function pushMessage($client_id=0,$function='')
    {
         // $command =' curl http://photosfromjerusalem.com/scan_park/ApiController/pushMessageExecute/'.$client_id.'/'.$function;
         // shell_exec($command);
        // $this->pushMessageExecute($client_id, $function);
        //file_get_contents(('http://photosfromjerusalem.com/scan_park/ApiController/pushMessageExecute/'.$client_id.'/'.$function));
        $array=array('client_id'=>$client_id,'text'=>$function,'parsed'=>0,'datetime'=>time());
        $this->db->insert('push_messages',$array);
    }

  
    
    public function getAppInfo()
    {
        echo json_encode(array('contact_email'=>'mohdsh85@gmail.com'));
    }
}
