<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of comments
 *
 * @author shareifhome
 */
class Comments extends CI_Controller{
     public function __construct() {
        parent::__construct();
        // $this->load->library('session');
      //   print_r($this->session);
    }
    
    public function _remap($method, $params = array())
    {
      //  echo 'ss'.$this->session->userdata[config('session_user_id')];
       // exit();

        if (method_exists($this, $method))
        {
                return call_user_func_array(array($this, $method), $params);
        }
        show_404();
    }    
  
    

    public function writeComment($imageId=18)
    {
        if(check_login())
        {
            $txt=$this->input->post('txt');
            $imageId=$this->input->post('imageId');
            $secureImage=$this->input->post('secureImage');
            if(secureToken($imageId)!=$secureImage || !$txt)
            {
                echo 0;
                return;
            }   
            $this->Writers_model->insertComment($imageId,$txt);
            $getComments=$this->Writers_model->getComment($imageId,0,0,true);;
            echo buildComments($getComments,$imageId);
            return;
        }
        echo 0;
        return;

    }
    public function loadMoreComments()
    {
            $imageId=$this->input->post('imageId');
            $secureImage=$this->input->post('secureImage');
            $offset=$this->input->post('offset');
            if(secureToken($imageId)!=$secureImage)            
            {
                echo 0;
                return;
            }
            
            $getComments=$this->Writers_model->getComment($imageId,$offset);;
            echo buildComments($getComments,$imageId);
           
    }
}
